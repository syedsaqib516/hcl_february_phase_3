package com.hcl.securityConfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@SuppressWarnings("deprecation")
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
	@Bean
	public BCryptPasswordEncoder getBean()
	{
		return new BCryptPasswordEncoder();
	}
	
   	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	
	auth.inMemoryAuthentication().withUser("admin1@gmail.com").password("{noop}admin1@123").authorities("ADMIN");
	auth.inMemoryAuthentication().withUser("user1@gmail.com").password("{noop}user1@123").authorities("CUST");
	auth.inMemoryAuthentication().withUser("user2@gmail.com").password("{noop}user2@123").authorities("EMP");
	
	}
   	
	@Override
	protected void configure(HttpSecurity http) throws Exception 
	{
		  //URL-Accss Type
		 http.authorizeRequests()
		 .antMatchers(HttpMethod.GET,"/home").permitAll()
		 .antMatchers("/welcome").authenticated()
		 .antMatchers("/admin").hasAuthority("ADMIN")
		 .antMatchers("/emp").hasAuthority("EMP")
		 .antMatchers("/cust").hasAuthority("CUST")
		  //LoginForm Detail
		 .and()
		 .formLogin()
		  .defaultSuccessUrl("/welcome",true)
		  // Logout Detail
		  
		  .and()
		  .logout()
		  .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
		  //exception Detail
		 
		 .and().exceptionHandling((exceptionHandling) ->
			exceptionHandling
				.accessDeniedPage("/error"));
			
			
	}

}

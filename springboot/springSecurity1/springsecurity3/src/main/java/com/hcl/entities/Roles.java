package com.hcl.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity

public class Roles {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	Integer roleId;
    @Column(unique=true)
	String role;
	
	public Roles() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Roles(Integer roleId, String role) {
		super();
		this.roleId = roleId;
		this.role = role;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
}

	
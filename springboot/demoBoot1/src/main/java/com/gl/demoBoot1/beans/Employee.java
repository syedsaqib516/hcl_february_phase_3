package com.gl.demoBoot1.beans;

public class Employee {

	private String ename;
	private String eaddr;
	private String esal;

	public String getEname() {
		return ename;
	}
	public Employee(String ename, String eaddr, String esal) {
		super();
		this.ename = ename;
		this.eaddr = eaddr;
		this.esal = esal;
	}
	public Employee() {
		super();
		
	}
	@Override
	public String toString() {
		return "Employee [ename=" + ename + ", eaddr=" + eaddr + ", esal=" + esal + "]";
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getEaddr() {
		return eaddr;
	}
	public void setEaddr(String eaddr) {
		this.eaddr = eaddr;
	}
	public String getEsal() {
		return esal;
	}
	public void setEsal(String esal) {
		this.esal = esal;
	}
	
	
}

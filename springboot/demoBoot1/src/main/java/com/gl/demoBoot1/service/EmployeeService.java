package com.gl.demoBoot1.service;

import com.gl.demoBoot1.beans.Employee;

public interface EmployeeService {
	Employee addEmployee(Employee emp);
}

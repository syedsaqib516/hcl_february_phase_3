package com.gl.demoBoot1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import com.gl.demoBoot1.controller.EmployeeController;

@SpringBootApplication
public class DemoBoot1Application implements ApplicationRunner// implements CommandLineRunner
{
	@Autowired
	Environment env;
	
	@Autowired
	EmployeeController controller;

	public static void main(String[] args) {
		SpringApplication.run(DemoBoot1Application.class, args);
	}
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		System.out.println(env.getProperty("message"));
		controller.addEmployee();
		
	}

//	@Override
//	public void run(String... args) throws Exception {
//		
//		System.out.println("hello world");
//		
//	}

	
	
	
	

}

package com.gl.demoBoot1.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.gl.demoBoot1.beans.Employee;

@Repository
public class EmployeeDAOImpl implements EmployeeDAO{
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public Employee addEmployee(Employee emp) {
		
		jdbcTemplate.update("insert into Employee values(?,?,?)",emp.getEname(),emp.getEaddr(),emp.getEsal());
		return emp;
	}

}

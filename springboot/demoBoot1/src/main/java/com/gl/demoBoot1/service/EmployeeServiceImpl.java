package com.gl.demoBoot1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.demoBoot1.beans.Employee;
import com.gl.demoBoot1.repository.EmployeeDAO;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeDAO dao;
	
	@Override
	public Employee addEmployee(Employee emp) {		
		dao.addEmployee(emp);
		return emp;
	}

}

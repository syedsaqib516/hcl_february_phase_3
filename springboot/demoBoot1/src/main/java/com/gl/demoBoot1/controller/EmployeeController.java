package com.gl.demoBoot1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.gl.demoBoot1.beans.Employee;
import com.gl.demoBoot1.service.EmployeeService;

@Controller
public class EmployeeController
{
	@Autowired
	EmployeeService service;
	
	public Employee addEmployee()
	{
		Employee emp = new Employee("syed@gmail","blr","1000");
		return service.addEmployee(emp);
	}

}

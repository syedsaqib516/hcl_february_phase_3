package com.hcl.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.model.AuthenticateRequest;
import com.hcl.model.Event;
import com.hcl.service.EventService;
import com.hcl.util.JwtUtil;

@RestController
@RequestMapping("/eve")
public class EventController 
{
	@Autowired
	EventService eventService;
	
	@Autowired
	JwtUtil jwtUtil;
	
	@CrossOrigin(origins="http://localhost:3000")
	@PostMapping("/addEvent/event")
	public ResponseEntity<?> addEvent(@RequestBody Event eve)
	{
		Event e=eventService.addEvent(eve);
		return new ResponseEntity<>(e,HttpStatus.CREATED);
	}
	
	@CrossOrigin(origins="http://localhost:3000")
	@GetMapping("/allevents")
	public ResponseEntity<?> getAllEvents()
	{
		List<Event> levent=eventService.getAllEvents();
		return new ResponseEntity<>(levent,HttpStatus.OK);
	}
	
	@GetMapping("/eventById/{eid}/id")
	public ResponseEntity<?> getEventById(@PathVariable("eid") int id)
	{
		Event e=eventService.getEventById(id);
		return new ResponseEntity<>(e,HttpStatus.OK);
	}
	
	@PostMapping("/getJwt")
	public String getJwt(@RequestBody AuthenticateRequest req)
	{
		if(req.getUname().equals("admin") && req.getPass().equals("hcl123"))
		{
			String token=jwtUtil.generateToken(req.getUname());
			return token;
		}
		else
		{
			return "Invalid Credentials! Please try again!";
		}	
	}	
}

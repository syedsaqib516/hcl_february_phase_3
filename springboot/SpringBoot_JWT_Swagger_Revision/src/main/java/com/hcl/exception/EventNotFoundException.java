package com.hcl.exception;


public class EventNotFoundException extends RuntimeException 
{
	private String msg;
	public EventNotFoundException(String msg)
	{
		super(msg);
		this.msg=msg;
	}
}


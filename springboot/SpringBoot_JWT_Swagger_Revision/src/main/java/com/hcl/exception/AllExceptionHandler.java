package com.hcl.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class AllExceptionHandler 
{
	@ExceptionHandler(value=EventAlreadyExistsException.class)
	@ResponseBody
	public ErrorResponse handleEventAlreadyExists(EventAlreadyExistsException ex)
	{
		ErrorResponse e=new ErrorResponse();
		e.setStatusCode(HttpStatus.CONFLICT.value());
		e.setMessage(ex.getMessage());
		return e;
	}
	@ExceptionHandler(value=EventNotFoundException.class)
	@ResponseBody
	public ErrorResponse handleEventNotFound(EventNotFoundException ex)
	{
		ErrorResponse e=new ErrorResponse();
		e.setStatusCode(HttpStatus.NOT_FOUND.value());
		e.setMessage(ex.getMessage());
		return e;
	}
	
}

package com.gl.daoTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.gl.dao.CustomerDAO;
import com.gl.model.Address;
import com.gl.model.Customer;

@SpringBootTest
@Transactional
public class CustomerDAOImplTest {

    @Autowired
    private CustomerDAO customerDAO;

    @Test
    @Order(1)
    public void testAddAndGetCustomer() {
        Customer customer = new Customer();
   //     customer.setCustomerId(1);
        customer.setName("Test");
        customer.setEmailId("test@test.com");
        Address address = new Address();
   //     address.setAddressId(1L);
        address.setCity("Test City");
        address.setStreet("Test Street");
        customer.setAddress(address);

        Integer customerId = customerDAO.addCustomer(customer);
        assertNotNull(customerId);

        Customer retrievedCustomer = customerDAO.getCustomer(customerId);
        assertNotNull(retrievedCustomer);
        assertEquals(customer.getName(), retrievedCustomer.getName());
        assertEquals(customer.getEmailId(), retrievedCustomer.getEmailId());
        assertEquals(customer.getAddress().getCity(), retrievedCustomer.getAddress().getCity());
        assertEquals(customer.getAddress().getStreet(), retrievedCustomer.getAddress().getStreet());
    }

    @Test
    @Order(2)
    public void testUpdateAddress() throws Exception {
        Customer customer = new Customer();
   //     customer.setCustomerId(1);
        customer.setName("Test");
        customer.setEmailId("test@test.com");
        Address address = new Address();
   //     address.setAddressId(1L);
        address.setCity("Test City");
        address.setStreet("Test Street");
        customer.setAddress(address);

        Integer customerId = customerDAO.addCustomer(customer);
        assertNotNull(customerId);

        Address newAddress = new Address();
        newAddress.setCity("New City");
        newAddress.setStreet("New Street");

        customerDAO.updateAddress(customerId, newAddress);

        Customer updatedCustomer = customerDAO.getCustomer(customerId);
        assertNotNull(updatedCustomer);
        assertEquals(newAddress.getCity(), updatedCustomer.getAddress().getCity());
        assertEquals(newAddress.getStreet(), updatedCustomer.getAddress().getStreet());
    }

    @Test
    @Order(3)
    public void testDeleteCustomer() {
        Customer customer = new Customer();
    //    customer.setCustomerId(1);
        customer.setName("Test");
        customer.setEmailId("test@test.com");
        Address address = new Address();
    //    address.setAddressId(1L);
        address.setCity("Test City");
        address.setStreet("Test Street");
        customer.setAddress(address);

        Integer customerId = customerDAO.addCustomer(customer);
        assertNotNull(customerId);

        customerDAO.deleteCustomer(customerId);
       
        assertNull(customerDAO.getCustomer(customerId));
    }
}


// package com.gl.daoTest;

// import static org.junit.Assert.assertEquals;
// import static org.junit.Assert.assertNotNull;
// import static org.junit.Assert.assertNull;
// import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

// import java.time.LocalDate;

// import javax.transaction.Transactional;

// import org.junit.Test;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.context.annotation.ComponentScan;
// import org.springframework.test.annotation.DirtiesContext;
// import org.springframework.test.context.TestPropertySource;

// import com.gl.dao.CustomerDAOImpl;
// import com.gl.model.Address;
// import com.gl.model.Customer;

// @SpringBootTest
// @Transactional
// @TestPropertySource(properties = "spring.jpa.properties.javax.persistence.validation.mode=none")
// @ComponentScan(basePackages = "com.gl.dao")
// public class CustomerDAOImplTest {


// 	@Autowired
// 	private CustomerDAOImpl customerDAO;

// 	@Test
// 	@Transactional
// 	@DirtiesContext
// 	public void testAddAndGetCustomer() {
// 		// Test adding a customer
// 		Customer customerToAdd = new Customer();
// 		customerToAdd.setName("John");
// 		customerToAdd.setEmailId("john@gl.com");
// 		customerToAdd.setDateOfBirth(LocalDate.of(1985, 5, 12));

// 		Address address = new Address();
// 		address.setCity("New York");
// 		address.setStreet("456 Main Street");

// 		customerToAdd.setAddress(address);

// 		assertNotNull(customerDAO);
// 		Integer addedCustomerId = customerDAO.addCustomer(customerToAdd);

// 		// Test getting the added customer
// 		Customer retrievedCustomer = customerDAO.getCustomer(addedCustomerId);

// 		// Assertions
// 		assertEquals(addedCustomerId, retrievedCustomer.getCustomerId());
// 		assertEquals("John", retrievedCustomer.getName());
// 		assertEquals("john@gl.com", retrievedCustomer.getEmailId());
// 		assertEquals(LocalDate.of(1985, 5, 12), retrievedCustomer.getDateOfBirth());
// 		assertEquals("New York", retrievedCustomer.getAddress().getCity());
// 		assertEquals("456 Main Street", retrievedCustomer.getAddress().getStreet());
// 	}

// 	@Test
// 	@Transactional
// 	@DirtiesContext
// 	public void testUpdateAddress() {
// 		// Test updating the address
// 		Address newAddress = new Address();
// 		newAddress.setCity("Los Angeles");
// 		newAddress.setStreet("789 Oak Avenue");

// 		// Assuming there is an existing customer with ID 1
// 		assertDoesNotThrow(() -> customerDAO.updateAddress(1, newAddress));

// 		// Test getting the updated customer
// 		Customer updatedCustomer = customerDAO.getCustomer(1);

// 		// Assertions
// 		assertEquals(Integer.valueOf(1), updatedCustomer.getCustomerId());
// 		assertEquals("John", updatedCustomer.getName());
// 		assertEquals("john@gl.com", updatedCustomer.getEmailId());
// 		assertEquals(LocalDate.of(1985, 5, 12), updatedCustomer.getDateOfBirth());
// 		assertEquals("Los Angeles", updatedCustomer.getAddress().getCity());
// 		assertEquals("789 Oak Avenue", updatedCustomer.getAddress().getStreet());
// 	}

// 	@Test
// 	@Transactional
// 	@DirtiesContext
// 	public void testDeleteCustomer() {
// 		// Test deleting the customer
// 		// Assuming there is an existing customer with ID 2
// 		assertDoesNotThrow(() -> customerDAO.deleteCustomer(2));

// 		// Test getting the deleted customer (should be null)
// 		Customer deletedCustomer = customerDAO.getCustomer(2);

// 		// Assertions
// 		assertNull(deletedCustomer);
// 	}
// }



























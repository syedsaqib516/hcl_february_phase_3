package com.gl.service;

import com.gl.model.Address;
import com.gl.model.Customer;


public interface CustomerService {

	public Customer getCustomer(Integer customerId) throws Exception;
	public Integer addCustomer(Customer customer);
	
	public void updateAddress(Integer customerId, Address newAddress) throws Exception;
	public void deleteCustomer(Integer customerId) throws Exception;
	
}

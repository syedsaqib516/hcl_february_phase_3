package com.gl.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.gl.entity.AddressEntity;
import com.gl.entity.CustomerEntity;
import com.gl.model.Address;
import com.gl.model.Customer;

@Repository(value = "customerDAO")
@Transactional
public class CustomerDAOImpl implements CustomerDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Customer getCustomer(Integer customerId) {
		CustomerEntity customerEntity  = entityManager.find(CustomerEntity.class, customerId);
		if(customerEntity!=null)
		{
			Customer customer = new Customer();
			customer.setCustomerId(customerEntity.getCustomerId());
			customer.setName(customerEntity.getName());
			customer.setEmailId(customerEntity.getEmailId());
			customer.setDateOfBirth(customerEntity.getDateOfBirth());
			Address address = new Address();
			address.setAddressId(customerEntity.getAddressEntity().getAddressId());
			address.setCity(customerEntity.getAddressEntity().getCity());
			address.setStreet(customerEntity.getAddressEntity().getStreet());
			customer.setAddress(address);
			return customer;
		}
		else
		{
			return null;
		}
	}

	@Override
	public Integer addCustomer(Customer customer) {
		System.out.println("entered add customer 1");
		CustomerEntity customerEntity = new CustomerEntity();
		System.out.println("entered add customer 2");
		customerEntity.setCustomerId(customer.getCustomerId());
		System.out.println("entered add customer 3");
		customerEntity.setDateOfBirth(customer.getDateOfBirth());
		System.out.println("entered add customer 4");
		customerEntity.setEmailId(customer.getEmailId());
		System.out.println("entered add customer 5");
		customerEntity.setName(customer.getName());
		System.out.println("entered add customer 6");

		AddressEntity addressEntity = new AddressEntity();
		System.out.println("entered add customer 7");
		addressEntity.setAddressId(customer.getAddress().getAddressId());
		System.out.println("entered add customer 8");
		addressEntity.setCity(customer.getAddress().getCity());
		System.out.println("entered add customer 9");
		addressEntity.setStreet(customer.getAddress().getStreet());
		System.out.println("entered add customer 10");

		customerEntity.setAddressEntity(addressEntity);
		System.out.println("entered add customer 11");
		entityManager.persist(customerEntity);
		System.out.println("entered add customer 12");
		return customerEntity.getCustomerId();
		//return 1;
	}

	@Override
	public void updateAddress(Integer customerId, Address newAddress) throws Exception {
		CustomerEntity customerEntity  = entityManager.find(CustomerEntity.class, customerId);
		AddressEntity addressEntity = customerEntity.getAddressEntity();
		addressEntity.setCity(newAddress.getCity());
		addressEntity.setStreet(newAddress.getStreet());

	}

	@Override
	public void deleteCustomer(Integer customerId) {
		CustomerEntity customerEntity  = entityManager.find(CustomerEntity.class, customerId);
		entityManager.remove(customerEntity);
	}
}
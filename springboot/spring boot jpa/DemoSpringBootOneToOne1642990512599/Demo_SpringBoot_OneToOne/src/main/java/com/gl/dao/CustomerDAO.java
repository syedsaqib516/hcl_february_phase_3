package com.gl.dao;

import com.gl.model.Address;
import com.gl.model.Customer;




public interface CustomerDAO {
	public Customer getCustomer(Integer customerId);
	public Integer addCustomer(Customer customer);
	
	public void updateAddress(Integer customerId, Address newAddress) throws Exception;
	public void deleteCustomer(Integer customerId);
}

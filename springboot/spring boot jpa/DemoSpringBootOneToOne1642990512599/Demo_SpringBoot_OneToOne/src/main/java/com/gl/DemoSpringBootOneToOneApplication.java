package com.gl;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.Environment;

import com.gl.model.Address;
import com.gl.model.Customer;
import com.gl.service.CustomerService;

@SpringBootApplication
@EnableAspectJAutoProxy
public class DemoSpringBootOneToOneApplication implements CommandLineRunner {

	@Autowired
	CustomerService customerService;

	@Autowired
	Environment environment;

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringBootOneToOneApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		addCustomer();
//		getCustomer();
//		updateAddress();
//		deleteCustomer();

	}

	public void getCustomer() {

		try {

			Customer customer = customerService.getCustomer(1);

			System.out.println("*****Customer Details*****");
			System.out.println("Customer ID: " + customer.getCustomerId());
			System.out.println("Name: " + customer.getName());
			System.out.println("Emailid: " + customer.getEmailId());
			System.out.println("DOB: " + customer.getDateOfBirth());

			System.out.println("Address id: " + customer.getAddress().getAddressId());
			System.out.println("Street: " + customer.getAddress().getStreet());
			System.out.println("City: " + customer.getAddress().getCity());

		} catch (Exception e) {
			String message = environment.getProperty(e.getMessage(),"Some exception occured. Please check log file for more details!!");

			System.out.println(message);
		}
	}

	public void addCustomer() {
		try {

			Customer customer = new Customer();

			customer.setName("Ron");
			customer.setEmailId("ron@gl.com");
			customer.setDateOfBirth(LocalDate.of(1993, 03, 24));

			Address address = new Address();
			address.setCity("Albany");
			address.setStreet("93 Taylor Road");

			customer.setAddress(address);

			Integer id = customerService.addCustomer(customer);
			System.out.println("\n" + environment.getProperty("UserInterface.CUSTOMER_ADDED") + id);

		} catch (Exception e) {
			String message = environment.getProperty(e.getMessage(),"Some exception occured. Please check log file for more details!!");

			System.out.println(message);
		}
	}

	public void updateAddress() {

		try {

			Address newAddress = new Address();
			newAddress.setCity("Rochester");
			newAddress.setStreet("12 Tim Street");

			customerService.updateAddress(1, newAddress);
			System.out.println("\n" + environment.getProperty("UserInterface.CUSTOMER_UPDATED"));

		} catch (Exception e) {
			String message = environment.getProperty(e.getMessage(),"Some exception occured. Please check log file for more details!!");

			System.out.println(message);
		}

	}

	public void deleteCustomer() {

		try {

			customerService.deleteCustomer(1);
			System.out.println("\n" + environment.getProperty("UserInterface.CUSTOMER_DELETED"));

		} catch (Exception e) {
			String message = environment.getProperty(e.getMessage(),"Some exception occured. Please check log file for more details!!");

			System.out.println(message);
		}
	}

}

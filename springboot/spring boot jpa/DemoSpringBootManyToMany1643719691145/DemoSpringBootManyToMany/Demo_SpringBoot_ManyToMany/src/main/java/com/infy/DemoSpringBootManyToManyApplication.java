package com.infy;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import com.infy.model.Service;
import com.infy.model.Customer;
import com.infy.service.BankService;

@SpringBootApplication
public class DemoSpringBootManyToManyApplication implements CommandLineRunner{

	@Autowired
	BankService service;

	@Autowired
	Environment environment;


	public static void main(String[] args) {
		SpringApplication.run(DemoSpringBootManyToManyApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		addCustomerAndService();
//		addExistingServiceToExistingCustomer();
//		deallocateServiceForExistingCustomer();		
//		deleteCustomer();
//		getCustomer();

	}

	public void addCustomerAndService() {
		try{
			Customer customer=new Customer();
			customer.setDateOfBirth(LocalDate.of(1995, 2, 1));
			customer.setEmailId("peter@infy.com");
			customer.setName("Peter");

			Set<Service> servicesList=new LinkedHashSet<Service>();


			Service services1=new Service();
			services1.setServiceId(3004);
			services1.setServiceName("Demat Services");
			services1.setServiceCost(200);

			servicesList.add(services1);

			customer.setBankServices(servicesList);

			Integer customerId=service.addCustomerAndService(customer);

			System.out.println(environment.getProperty("UserInterface.NEW_CUSTOMER_SUCCESS")+customerId);

		}catch(Exception e){
			String message = environment.getProperty(e.getMessage(),"Some exception occured. Please check log file for more details!!");
			System.out.println( message);
		}


	}

	public void addExistingServiceToExistingCustomer() {
		try{
			Integer customerId=1004;
			List<Integer> serviceIds=new ArrayList<>();
			serviceIds.add(3001);
			serviceIds.add(3003);

			service.addExistingServiceToExistingCustomer(customerId, serviceIds);
			System.out.println(environment.getProperty("UserInterface.CUSTOMER_SERVICE_ALLOCATION_SUCCESS"));

		}catch(Exception e){
			String message = environment.getProperty(e.getMessage(),"Some exception occured. Please check log file for more details!!");
			System.out.println(message);
		}

	}

	public void deallocateServiceForExistingCustomer() {
		try{
			Integer customerId=1002;
			List<Integer> serviceIds=new ArrayList<>();
			serviceIds.add(3003);
			service.deallocateServiceForExistingCustomer(customerId, serviceIds);
			System.out.println(environment.getProperty("UserInterface.CUSTOMER_SERVICE_DEALLOCATION_SUCCESS"));

		}catch(Exception e){
			String message = environment.getProperty(e.getMessage(),"Some exception occured. Please check log file for more details!!");
			System.out.println(message);
		}

	}

	public void deleteCustomer() {
		try{

			Integer customerId=1001;

			service.deleteCustomer(customerId);

			System.out.println(environment.getProperty("UserInterface.CUSTOMER_DELETE_SUCCESS"));


		}catch(Exception e){
			String message = environment.getProperty(e.getMessage(),"Some exception occured. Please check log file for more details!!");
			System.out.println(message);
		}

	}

	public void getCustomer() {
		try{

			Integer customerId=1004;

			Customer customer=service.getCustomer(customerId);
			System.out.println("Customer Details:");
			System.out.println("--------------------------------");
			System.out.println("Customer Id:\t"+customer.getCustomerId());
			System.out.println("Customer Name:\t"+customer.getName());
			Set<Service> serviceList=customer.getBankServices();
			if(serviceList!=null && !serviceList.isEmpty()){
				System.out.println("\n\nService Details\n---------------");
				System.out.println("---------------");
				for (Service s : serviceList) {
					System.out.println("Service Name:\t"+s.getServiceName());
					System.out.println("Service Cost:\t"+s.getServiceCost());
					System.out.println("--------------------------------");
				}
			}else{
				System.out.println(environment.getProperty("UserInterface.SERVICE_UNAVAILABLE"));
			}
		}catch(Exception e){
			String message = environment.getProperty(e.getMessage(),"Some exception occured. Please check log file for more details!!");
			System.out.println(message);
		}
	}
}


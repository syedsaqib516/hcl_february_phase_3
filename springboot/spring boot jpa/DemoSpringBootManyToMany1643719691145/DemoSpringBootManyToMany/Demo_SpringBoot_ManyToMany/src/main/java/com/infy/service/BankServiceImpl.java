package com.infy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.infy.dao.BankDAO;
import com.infy.model.Customer;


@Service(value = "bankService")
@Transactional
public class BankServiceImpl implements BankService {
	
	@Autowired
	private BankDAO bankDAO;

	@Override
	public Integer addCustomerAndService(Customer customer) throws Exception {
		// TODO Auto-generated method stub
		return bankDAO.addCustomerAndService(customer);
	}

	@Override
	public void addExistingServiceToExistingCustomer(Integer customerId,
			List<Integer> serviceIds) throws Exception {
		
		Customer customer=bankDAO.getCustomer(customerId);
		
		if(customer==null){
			throw new Exception("Service.CUSTOMER_UNAVAILABLE");
		}
		
		for (Integer serviceId : serviceIds) {
			if(bankDAO.getService(serviceId)==null)
				throw new Exception("Service.SERVICE_UNAVAILABLE");
		}
		
		bankDAO.addExistingServiceToExistingCustomer(customerId, serviceIds);
	}
	
	
	@Override
	public void deallocateServiceForExistingCustomer(Integer customerId,
			List<Integer> serviceIds) throws Exception {
		
		Customer customer=bankDAO.getCustomer(customerId);
		if(customer==null){
			throw new Exception("Service.CUSTOMER_UNAVAILABLE");
		}
		
		for (Integer serviceId : serviceIds) {
			if(bankDAO.getService(serviceId)==null)
				throw new Exception("Service.SERVICE_UNAVAILABLE");
		}
		
		bankDAO.deallocateServiceForExistingCustomer(customerId, serviceIds);
	}
	
	
	@Override
	public void deleteCustomer(Integer customerId) throws Exception {
		
		Customer customer=bankDAO.getCustomer(customerId);
		
		if(customer==null){
			throw new Exception("Service.CUSTOMER_CANNOT_DELETE");
		}
		
		bankDAO.deleteCustomer(customerId);;
		
	}
	
	
	@Override
	public Customer getCustomer(Integer customerId) throws Exception {
		
		Customer customer=bankDAO.getCustomer(customerId);
		if(customer==null){
			throw new Exception("Service.CUSTOMER_UNAVAILABLE");
		}
		return customer;
	}
	
}
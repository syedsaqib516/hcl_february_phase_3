package com.infy.service;

import java.util.List;



import com.infy.model.Customer;


public interface BankService {
	public Integer addCustomerAndService(Customer customer) throws Exception;
	public void addExistingServiceToExistingCustomer(Integer customerId,List<Integer> serviceIds) throws Exception;
	public void deallocateServiceForExistingCustomer(Integer customerId,List<Integer> serviceIds) throws Exception;		
	public void deleteCustomer(Integer customerId) throws Exception;
	public Customer getCustomer(Integer customerId) throws Exception;
}


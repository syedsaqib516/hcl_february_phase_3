drop table service cascade constraints;
drop table customer_service cascade constraints;
drop table customer cascade constraints;
drop sequence hibernate_sequence;
create sequence hibernate_sequence start with 1006 increment by  1;
create table service (
	service_id number(10) not null,
	service_name varchar2(20),
	service_cost number(5),
	primary key (service_id)
);
create table customer (
	customer_id number(10) not null,
	date_of_birth date,
	emailid varchar2(20),
	name varchar2(20),
	primary key (customer_id)
);
create table customer_service (
	cust_id number(10) not null,
	serv_id number(10) not null
);

alter table customer_service add constraint fk_service_mapping foreign key (serv_id) references service(service_id);
alter table customer_service add constraint fk_customer_mapping foreign key (cust_id) references customer(customer_id);


INSERT INTO customer (customer_id, emailid, name, date_of_birth) VALUES (1001,'steven@infy.com', 'Steven Martin', SYSDATE-7476);
INSERT INTO customer (customer_id, emailid, name, date_of_birth) VALUES (1002,'kevin@infy.com', 'Kevin Nelson', SYSDATE-11374);
INSERT INTO customer (customer_id, emailid, name, date_of_birth) VALUES (1003,'john@infy.com', 'John Matric', SYSDATE-12344);
INSERT INTO customer (customer_id, emailid, name, date_of_birth) VALUES (1004,'chan@infy.com', 'Chan mathew', SYSDATE-10344);
INSERT INTO customer (customer_id, emailid, name, date_of_birth) VALUES (1005,'jill@infy.com', 'Jill mathew', SYSDATE-11374);


INSERT INTO service (service_id, service_name, service_cost) values (3001,'Internet Banking',15);
INSERT INTO service (service_id, service_name, service_cost) values (3002,'Phone Banking',10);
INSERT INTO service (service_id, service_name, service_cost) values (3003,'Mobile Banking',20);



INSERT INTO customer_service (cust_id,serv_id) values (1001,3001);
INSERT INTO customer_service (cust_id,serv_id) values (1001,3002);
INSERT INTO customer_service (cust_id,serv_id) values (1002,3003);
INSERT INTO customer_service (cust_id,serv_id) values (1003,3001);
INSERT INTO customer_service (cust_id,serv_id) values (1003,3003);

commit;
select * from customer;
select * from service;
select * from customer_service;

package com.infy.dao;

import com.infy.model.Customer;
import com.infy.model.Loan;



public interface CustomerLoanDAO {

	public Loan getLoanDetails(Integer loanId);
	public Integer addLoanAndCustomer(Loan loan);
	public Integer sanctionLoanToExistingCustomer(Integer customerId,Loan loan);
	public void closeLoan(Integer loanId);
	public Customer getCustomerDetails(Integer customerId);
    public void deleteLoan(Integer loanId);
}
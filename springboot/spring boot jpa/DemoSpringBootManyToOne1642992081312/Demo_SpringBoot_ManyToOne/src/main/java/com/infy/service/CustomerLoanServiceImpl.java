package com.infy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.infy.dao.CustomerLoanDAO;
import com.infy.model.Customer;
import com.infy.model.Loan;

@Repository(value = "customerLoanService")
@Transactional
public class CustomerLoanServiceImpl implements CustomerLoanService {

	@Autowired
	private CustomerLoanDAO loanDAO;

	@Override
	public Loan getLoanDetails(Integer loanId) throws Exception {

		Loan loan = loanDAO.getLoanDetails(loanId);
		if (loan == null) {
			throw new Exception("Service.LOAN_UNAVAILABLE");
		}
		return loan;
	}

	@Override
	public Integer addLoanAndCustomer(Loan loan) throws Exception {

		return loanDAO.addLoanAndCustomer(loan);
	}

	@Override
	public Integer sanctionLoanToExistingCustomer(Integer customerId, Loan loan) throws Exception {
		Customer customer = loanDAO.getCustomerDetails(customerId);
		if(customer==null){
			throw new Exception("Service.CUSTOMER_UNAVAILABLE");
		}
		return loanDAO.sanctionLoanToExistingCustomer(customerId, loan);

	}

	@Override
	public void closeLoan(Integer loanId) throws Exception {
		Loan loan = loanDAO.getLoanDetails(loanId);
		if (loan == null) {
			throw new Exception("Service.LOAN_UNAVAILABLE");
		}
		loanDAO.closeLoan(loanId);

	}
	
	public void deleteLoan(Integer loanId) throws Exception{
		Loan loan = loanDAO.getLoanDetails(loanId);
		if (loan == null) {
			throw new Exception("Service.LOAN_UNAVAILABLE");
		}
		loanDAO.deleteLoan(loanId);
	}

}
package com.infy.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.infy.entity.CustomerEntity;
import com.infy.entity.LoanEntity;
import com.infy.model.Customer;
import com.infy.model.Loan;

@Repository(value = "customerLoanDAO")
public class CustomerLoanDAOImpl implements CustomerLoanDAO {

	@PersistenceContext
	private EntityManager entityManager;

	// fetches loan details
	@Override
	public Loan getLoanDetails(Integer loanId){
		LoanEntity loanEntity = entityManager.find(LoanEntity.class, loanId);
		Loan loan = null;
		if (loanEntity != null) {
			loan = new Loan();
			loan.setAmount(loanEntity.getAmount());
			loan.setLoanId(loanEntity.getLoanId());
			loan.setLoanIssueDate(loanEntity.getIssueDate());
			loan.setStatus(loanEntity.getStatus());
			CustomerEntity customerEntity = loanEntity.getCustomer();
			if (customerEntity != null) {
				Customer customer = new Customer();
				customer.setCustomerId(customerEntity.getCustomerId());
				customer.setDateOfBirth(customerEntity.getDateOfBirth());
				customer.setEmailId(customerEntity.getEmailId());
				customer.setName(customerEntity.getName());
				loan.setCustomer(customer);
			}
		}
		return loan;
	}

	// sanction loan to a new customer
	@Override
	public Integer addLoanAndCustomer(Loan loan){
		LoanEntity loanEntity = new LoanEntity();
		loanEntity.setAmount(loan.getAmount());
		loanEntity.setIssueDate(loan.getLoanIssueDate());
		loanEntity.setStatus("open");
		Customer customer = loan.getCustomer();
		CustomerEntity customerEntity = new CustomerEntity();
		customerEntity.setCustomerId(customer.getCustomerId());
		customerEntity.setDateOfBirth(customer.getDateOfBirth());
		customerEntity.setEmailId(customer.getEmailId());
		customerEntity.setName(customer.getName());
		loanEntity.setCustomer(customerEntity);
		entityManager.persist(loanEntity);
		return loanEntity.getLoanId();
	}

	// sanction loan to existing customer
	@Override
	public Integer sanctionLoanToExistingCustomer(Integer customerId, Loan loan) {
		LoanEntity loanEntity = new LoanEntity();
		loanEntity.setAmount(loan.getAmount());
		loanEntity.setIssueDate(loan.getLoanIssueDate());
		loanEntity.setStatus(loan.getStatus());
		CustomerEntity customerEntity = entityManager.find(CustomerEntity.class, customerId);
		loanEntity.setCustomer(customerEntity);
		entityManager.persist(loanEntity);
		return loanEntity.getLoanId();

	}
	
	// closes the loan
	@Override
	public void closeLoan(Integer loanId) {
		LoanEntity loanEntity = entityManager.find(LoanEntity.class, loanId);
		loanEntity.setStatus("Closed");
	}
	
	@Override
	public Customer getCustomerDetails(Integer customerId) {
		Customer customer = null;
		CustomerEntity customerEntity = entityManager.find(CustomerEntity.class,customerId);
		if(customerEntity!=null){
			customer = new Customer();
			customer.setCustomerId(customerEntity.getCustomerId());
			customer.setDateOfBirth(customerEntity.getDateOfBirth());
			customer.setEmailId(customerEntity.getEmailId());
			customer.setName(customer.getName());
		}
		
		return customer;
	}
	
	@Override
	public void deleteLoan(Integer loanId){
		LoanEntity loanEntity = entityManager.find(LoanEntity.class, loanId);
		loanEntity.setCustomer(null);
		entityManager.remove(loanEntity);
		
	}
}
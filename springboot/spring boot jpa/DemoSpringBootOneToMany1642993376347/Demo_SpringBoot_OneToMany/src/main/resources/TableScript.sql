DROP TABLE Card CASCADE CONSTRAINTS;
DROP TABLE Customer CASCADE CONSTRAINTS;
DROP SEQUENCE hibernate_sequence;
CREATE SEQUENCE hibernate_sequence start with 1006 increment by  1;

create table Customer (
  customer_id number(10) not null,
  emailid varchar2(20),
  name varchar2(20),
  date_of_birth date, 
  CONSTRAINT customer_pk PRIMARY KEY (customer_id)
);

INSERT INTO Customer VALUES (1001,'steven@infy.com', 'Steven Martin', SYSDATE-7476);
INSERT INTO Customer VALUES (1002,'kevin@infy.com', 'Kevin Nelson', SYSDATE-11374);
INSERT INTO Customer VALUES (1003,'john@infy.com', 'John Matric', SYSDATE-12344);
INSERT INTO Customer VALUES (1004,'chan@infy.com', 'Chan Mathew', SYSDATE-10344);
INSERT INTO Customer VALUES (1005,'jill@infy.com', 'Jill Mathew', SYSDATE-11374);

CREATE TABLE Card (
  card_id number(10) not null,
  card_number varchar2(20),
  expiry_date date,
  cust_id number(10),
  CONSTRAINT card_pk PRIMARY KEY (card_id),
  CONSTRAINT fk_card_cust FOREIGN KEY (cust_id) REFERENCES Customer
);



INSERT INTO Card VALUES(12346, '6642160005012193',SYSDATE+3400,1001);
INSERT INTO Card VALUES(12347, '6642160005012194',SYSDATE+4560,1001);
INSERT INTO Card VALUES(12348, '6642160005012195',SYSDATE+1160,1001);
INSERT INTO Card VALUES(12349, '6642160005012196',SYSDATE+5660,1002);
INSERT INTO Card VALUES(12350, '6642160005012197',SYSDATE+5640,1003);
INSERT INTO Card VALUES(12351, '6642160005012198',SYSDATE+5620,1003);



commit;


select * from Card;
select * from Customer;

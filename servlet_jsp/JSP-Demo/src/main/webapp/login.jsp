<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="Error.jsp"%>
    
<!--  //declarative tag-->
 <%@ page import="java.io.PrintWriter" %>  
  <%@ page import="beans.Customer" %>   

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
     <!-- // declaration tag -->
     <%! 
     String username;
     String password;
     
     int cube(int n)
     {  
    	 return n*n*n ;  
     }
     
     %>  
     <!-- //scriptlet tag -->
     <%   
      username=request.getParameter("username");
      password=request.getParameter("password");
   
    
     if(username.equals("syed@gmail.com") && password.equals("syed@123"))
     {
    	 pageContext.setAttribute("user",username,PageContext.SESSION_SCOPE); 
    	 
    	 out.print("<h1>Welcome "+ username +" To JSP !</h1>");
     }
     else
     {
    	 out.print("<h1>Error "+ username +" using invalid password !</h1>");
    	 
     }
     
     %>  
    <!--  // expression tag -->
     
    <%= "welcome to jsp" +cube(10)%>  
    
     <jsp:forward page="printdate.jsp" />   
     
     <%-- <jsp:include page="printdate.jsp" />   --%>
     <jsp:useBean id="customer" class="beans.Customer"></jsp:useBean> 
     <jsp:setProperty name="customer" property="username" value="syed@gmail.com" />  

      
     
     
</body>
</html>
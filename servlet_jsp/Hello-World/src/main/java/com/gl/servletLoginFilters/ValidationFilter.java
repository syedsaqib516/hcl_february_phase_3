package com.gl.servletLoginFilters;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;

/**
 * Servlet Filter implementation class ValidationFilter
 */
@WebFilter("/login")
public class ValidationFilter extends HttpFilter implements Filter {
       
   
	public void destroy() {
		System.out.println("filter destroyed");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	
		// place your code here
		HttpServletRequest req= (HttpServletRequest)request;
		PrintWriter out = response.getWriter();
		 String username=req.getParameter("username");
		 if(username.contains("@gmail.com"))	     
		// pass the request along the filter chain
		chain.doFilter(request, response);
		 else
			 out.print("invalid username format");
	}

	
	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("filter initialized");
	}

}

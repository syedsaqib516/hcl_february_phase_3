package com.gl.servletLogin;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/welcome")
public class WelcomeServlet extends HttpServlet 
{

	public void doGet(HttpServletRequest req,HttpServletResponse res) throws IOException
	{
		PrintWriter out = res.getWriter();
	//	ServletContext ctx= getServletContext();
	//	String name= ctx.getInitParameter("name");   

		HttpSession session=  req.getSession();
		Cookie[] cookies=req.getCookies();
		String username=null;
		for(Cookie c:cookies)
		{
			if(c.getName().equals("username"))
				username=c.getValue();
		}
		//String username=(String) session.getAttribute("username");

		//String username=req.getParameter("username");

		out.print("<h1>Welcome "+username +" to Servlet Programming</h1>");//username


	}

}

package com.gl.gltel.customer.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gl.gltel.customer.dto.CustomerDTO;
import com.gl.gltel.customer.dto.LoginDTO;
import com.gl.gltel.customer.dto.PlanDTO;
//import com.gl.gltel.customer.service.CustCircuitBreakerService;
import com.gl.gltel.customer.service.CustomerService;

@RestController
@CrossOrigin
public class CustomerController {

	Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	CustomerService custService;
	
//	@Autowired
//	CustCircuitBreakerService custCircuitService;
	
	@Autowired
	CustPlanFeign custPlanFeign;
	
	@Autowired
	CustFriendFeign custFriendFeign;
	
	@Value("${friend.uri}")
	String friendUri;

	@Value("${plan.uri}")
	String planUri;

	
	// Create a new customer
	@PostMapping(value = "/customers",  consumes = MediaType.APPLICATION_JSON_VALUE)
	public void createCustomer(@RequestBody CustomerDTO custDTO) {
		logger.info("Creation request for customer "+ custDTO);
		custService.createCustomer(custDTO);
	}

	// Login
	@PostMapping(value = "/login",consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean login(@RequestBody LoginDTO loginDTO) {
		logger.info("Login request for customer "+loginDTO.getPhoneNo()+" with password "+loginDTO.getPassword());
		return custService.login(loginDTO);
	}

	
	// Fetches full profile of a specific customer
		@GetMapping(value = "/customers/{phoneNo}",  produces = MediaType.APPLICATION_JSON_VALUE)
		public CustomerDTO getCustomerProfile(@PathVariable Long phoneNo) {
			logger.info("Profile request for customer " +phoneNo);
			CustomerDTO custDTO=custService.getCustomerProfile(phoneNo);
			
			PlanDTO planDTO= custPlanFeign.getSpecificPlan(custDTO.getCurrentPlan().getPlanId());
			
			List<Long> friends= custFriendFeign.getSpecificFriends(phoneNo);
			
			custDTO.setCurrentPlan(planDTO);
			custDTO.setFriendAndFamily(friends);
			
			return custDTO;
		}
//	public CustomerDTO getCustomerProfileFallback(Long phoneNo, Throwable throwable) {
//		logger.info("============ In Fallback =============");
//		return new CustomerDTO();
//	}


}

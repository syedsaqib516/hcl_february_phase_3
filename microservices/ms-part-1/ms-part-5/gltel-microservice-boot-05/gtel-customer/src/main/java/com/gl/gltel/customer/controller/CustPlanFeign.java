package com.gl.gltel.customer.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.gl.gltel.customer.dto.PlanDTO;

@FeignClient(name = "PlanMS", url="http://localhost:9400/")
public interface CustPlanFeign {
	
	@GetMapping("/plans/{planId}")
	PlanDTO getSpecificPlan(@PathVariable("planId") int planId);

}

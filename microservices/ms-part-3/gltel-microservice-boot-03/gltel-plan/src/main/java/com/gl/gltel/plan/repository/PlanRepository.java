package com.gl.gltel.plan.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.gltel.plan.entity.Plan;

public interface PlanRepository extends JpaRepository<Plan, Integer> {

}

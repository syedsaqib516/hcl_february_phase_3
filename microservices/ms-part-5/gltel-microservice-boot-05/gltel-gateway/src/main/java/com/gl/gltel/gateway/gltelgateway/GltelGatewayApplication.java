package com.gl.gltel.gateway.gltelgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient	
public class GltelGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GltelGatewayApplication.class, args);
	}

}

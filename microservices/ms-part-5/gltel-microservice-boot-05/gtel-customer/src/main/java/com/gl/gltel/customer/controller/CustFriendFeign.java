package com.gl.gltel.customer.controller;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

	@FeignClient(name = "FriendMS", url="http://localhost:9300/")
	public interface CustFriendFeign {
		
		@GetMapping("/customers/{phoneNo}/friends")
		List<Long> getSpecificFriends(@PathVariable("phoneNo") Long phoneNo);

	}


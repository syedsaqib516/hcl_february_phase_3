package com.gl.beansXmlBased;

import java.util.stream.Stream;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	public static void main(String[] args) 
	
	{
		
//		Address addr1= new Address("x street","y city","90890",Stream.of("x lm","y lm").toList());
//		
//		Employee emp1 = new Employee("101","syed","syed@gmail",addr1);
//		
//		System.out.println(emp1);
		 
		ApplicationContext context =new ClassPathXmlApplicationContext("config.xml");
		Employee emp1=(Employee) context.getBean("emp1");
		Employee emp2=context.getBean(Employee.class);
		System.out.println(emp1);
		System.out.println(emp1.getName());
		emp1.welcome();
		
		
	}
}

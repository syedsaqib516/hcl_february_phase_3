package com.gl.servicesJavaBased;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

public class ReportService {
    // @Autowired  // For initializing only object dependency using xml
    //  @Qualifier("pdfReportGenerator")
	
    private ReportGenerator master;
    
    @Value("100") // Annotation for initializing primitive types
    private int recordsPerPage;
    
    public ReportService() {
        System.out.println("default constructor");
    }
    
    public ReportService(ReportGenerator master) {
        System.out.println("constructor");
        this.master = master;
    }
    public ReportService(ReportGenerator master, int recordsPerPage) {
        super();
        System.out.println("Parameterized Constructor");
        this.master = master;
        this.recordsPerPage = recordsPerPage;
    }

	public int getRecordsPerPage() {
        return recordsPerPage;
    }
    public void setRecordsPerPage(int recordsPerPage) {
        this.recordsPerPage = recordsPerPage;
    }
    public ReportGenerator getMaster() {
        return master;
    }
        
    public void setMaster(ReportGenerator master) {
        this.master = master;
    }
    public void generateReport() {
        System.out.println(master.generateReport(recordsPerPage));
    }
}

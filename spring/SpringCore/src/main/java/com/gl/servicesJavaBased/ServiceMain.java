package com.gl.servicesJavaBased;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ServiceMain {
	public static void main(String[] args) {
		 ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		  ReportService srv1 = (ReportService)context.getBean("reportService1");
	        srv1.generateReport();        
	        ReportService srv2 = (ReportService)context.getBean("reportService2");
	        srv2.generateReport();
	}

}

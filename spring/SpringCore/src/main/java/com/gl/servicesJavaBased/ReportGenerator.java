package com.gl.servicesJavaBased;

public interface ReportGenerator {

	 public String generateReport(int recordsPerPage);

}

package com.gl.AnnotationBased;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.gl.AnnotationBased")
public class AppConfig {

}

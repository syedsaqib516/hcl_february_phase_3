package com.gl.AnnotationBased;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ServiceMain {
	public static void main(String[] args) {
		 ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		  ReportService srv1 = (ReportService)context.getBean("reportService");
	        srv1.generateReport();        
	      
	}

}

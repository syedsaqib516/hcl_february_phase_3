package com.gl.AnnotationBased;

public interface ReportGenerator {

	 public String generateReport(int recordsPerPage);

}

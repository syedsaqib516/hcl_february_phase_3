package com.gl.AnnotationBased;

import org.springframework.stereotype.Component;

@Component(value="pdfReportGenerator")
public class PDFReportGenerator implements ReportGenerator {

	@Override
	public String generateReport(int recordsPerPage) {
		
		 return "Generated PDF Report with " + recordsPerPage + " records";
	}

}

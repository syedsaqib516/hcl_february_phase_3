package com.gl.test;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import com.gl.main.VoterValidator;

public class VoterValidatorTest {

	public static int counter;
	public static VoterValidator voterValidator;
	
	@BeforeAll
	public static void beforeMethod() {
		 voterValidator= new VoterValidator();
		 System.out.println("Object Created new VoterValidator();");
	}
	
	@BeforeEach
	public void beforeEachMethod() {
		 voterValidator= new VoterValidator();
		 System.out.println("Object Created new VoterValidator();");
		System.out.println("Before test case "+(++counter));
	}	
	@Tag("tst")
	@Test
	@Order(1)
	public void validateVoterAgeValidTest() throws Exception {
		int age = 18;
		
		boolean expectedResult = true;
		boolean actualResult = voterValidator.validateVoterAge(age);
		// Assert.assertEquals(expectedResult, actualResult);
		Assertions.assertTrue(actualResult);
	}
	
	@Tag("dev")
	@Test
	@Order(2)
	public void validateVoterAgeInvalidTest() throws Exception {
		int age = 17;
		
		boolean result = voterValidator.validateVoterAge(age);
		Assertions.assertFalse(result);
	}
	
	@Tag("prod")
	@Test
	@Order(3)
	public void validateVoterAgeInvalidTestWithException() throws Exception {
		int age = -14;
		
		Exception exception = Assertions.assertThrows(Exception.class, () -> voterValidator.validateVoterAge(age));
		Assertions.assertEquals("Invalid age", exception.getMessage());
	}

	
	@ParameterizedTest
	@CsvSource(value = {"19, true","20,true","17,false","16,false"})
	@Order(4)
	public void validateVoterAgeTestParameter(int age, boolean expected) throws Exception {
		
		boolean actual = voterValidator.validateVoterAge(age);
		Assertions.assertEquals(expected, actual);
	}
	
	@AfterEach
	public void afterEachMethod() {
		 voterValidator=null;
	     System.out.println("Object Destroyed new VoterValidator();");
		System.out.println("After test case "+(counter));
	}
	
	@AfterAll
	public static void afterMethod() {
	     voterValidator=null;
	     System.out.println("Object Destroyed new VoterValidator();");
	}
	
	



}

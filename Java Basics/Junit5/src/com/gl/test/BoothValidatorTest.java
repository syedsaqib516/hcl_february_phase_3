package com.gl.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import com.gl.main.BoothValidator;

public class BoothValidatorTest {
	
    @Tag("prod")
    @Order(1)
	@Test
	public void validateBoothDetailsValidTest1() throws Exception {
		String boothName = "Booth1";
		BoothValidator boothValidator = new BoothValidator();
		Assertions.assertTrue(boothValidator.validateBoothDetails(boothName));
		System.out.println(1);
	}
	
    @Tag("dev")
    @Order(2)
	@Test
	public void validateBoothDetailsInvalidTest2() throws Exception {
		String boothName = "";
		BoothValidator boothValidator = new BoothValidator();
		Assertions.assertFalse(boothValidator.validateBoothDetails(boothName));
		System.out.println(2);
	}
    @Tag("dev")	
    @Order(3)
	@Test
	public void validateBoothDetailsInvalidTest3() throws Exception {
		String boothName = null;
		BoothValidator boothValidator = new BoothValidator();
		Exception exception = Assertions.assertThrows(Exception.class, () -> boothValidator.validateBoothDetails(boothName));
		Assertions.assertEquals("Booth name is invalid.", exception.getMessage());
		System.out.println(3);
	}
}


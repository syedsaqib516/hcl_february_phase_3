package com.gl.multithreading;

import java.util.Scanner;

class Banking extends Thread
{
	@Override
	public void run()
	{
		System.out.println("banking ativity started");
		Scanner sc = new Scanner(System.in);
		System.out.println("enter acc no: ");
		int acc=sc.nextInt();
		System.out.println("enter password :");
		int pwd=sc.nextInt();
		System.out.println("enter amt to withdraw :");
		int amt=sc.nextInt();
		System.out.println("amt of "+amt+" successfully withdrawn");
		System.out.println("banking ativity ended");
		
	}
	
}
class Transactions  extends Thread
{
	@Override
	public void run()
	{
		System.out.println("tranction ativity started");
		for (int i = 0; i < 5; i++) {
			System.out.println(" transaction "+i);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("transaction ativity ended");
		
	}
}
class Comments extends Thread
{
	@Override
	public void run() 
	{
		System.out.println("comments ativity started");
		for (int i = 97; i < 103; i++) {
			System.out.println(" Comments "+(char)i);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("comments ativity ended");
		
	}
}



public class Demo3
{
	public static void main(String[] args) 
	{
		Banking thread1 = new Banking();
		Transactions thread2= new Transactions();
		Comments thread3= new Comments();
		thread1.start();
		thread2.start();
		thread3.start();

	}

}

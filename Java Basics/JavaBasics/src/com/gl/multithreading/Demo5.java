package com.gl.multithreading;

class Resource implements Runnable
{

	@Override
	synchronized public void run() {
		System.out.println(Thread.currentThread().getName()+" has entered ");
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(Thread.currentThread().getName()+" is using the resource ");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(Thread.currentThread().getName()+" exited the resource ");
		
	}
	
}


public class Demo5 
{
   public static void main(String[] args) throws InterruptedException {
	   Resource r = new Resource();
	   Thread t1=new Thread(r);
	   Thread t2=new Thread(r);
	   Thread t3=new Thread(r);
	   t1.setName("thread 1");
	   t2.setName("thread 2");
	   t3.setName("thread 3");
	   t1.start();
	 //  t1.join();
	   t2.start();
	 //  t2.join();
	   t3.start();
	 //   t3.join();
}
}

package com.gl.multithreading;



class Queue
{
	int x ;
	boolean valueIsPresent=false; 
	synchronized public void put(int j)
	{
		if(valueIsPresent==true)
		{  
			try {
				wait();// communicating with thread flow
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		else
		{
			x=j;
			System.out.println(" I have put into x "+x);
			notify();
			valueIsPresent=true;
		}

	}
 
	synchronized public void get()
	{
		if(valueIsPresent==false)
		{  
			try {
					wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		else
		{
			System.out.println(" I Have taken from x "+x);
			valueIsPresent=false;
			notify();
		}
	}
}

class Producer extends Thread
{
	Queue q;


	Producer(Queue q)
	{
		this.q=q;
	}
	public void run()
	{
		int i=1;

		while(true)
		{
			q.put(i++);
		}
	}
}
class Consumer extends Thread
{
	Queue q;


	Consumer (Queue q)
	{
		this.q=q;
	}
	public void run()
	{


		while(true)
		{
			q.get();
		}
	}
}

public class Demo7 
{

	public static void main(String[] args) {
		{
			Queue q= new Queue();
			Producer p=new Producer(q);
			Consumer c=new Consumer(q);
			p.start();
			c.start();


		}
	}
}

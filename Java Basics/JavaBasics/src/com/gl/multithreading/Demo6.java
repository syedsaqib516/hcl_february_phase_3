package com.gl.multithreading;

class CounterStrike extends Thread
{
	String weapon1="maverick";
	String weapon2="ak-47";
	String weapon3="grenade";
	
	public void run()
	{
		if(getName().equals("raunaq"))
		{
			try {
				raunaqAccWeapons();
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}
		if(getName().equals("kishore"))
		{
			try {
				kishoreAccWeapons();
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}
	}

	private void raunaqAccWeapons() throws InterruptedException {
		synchronized(weapon1)
		{
			System.out.println(" raunaq has accuired "+weapon1);
			Thread.sleep(5000);
			synchronized(weapon2)
			{
				System.out.println(" raunaq has accuired "+weapon2);
				Thread.sleep(5000);
				synchronized(weapon3)
				{
					System.out.println("raunaq has accuired "+weapon3);
					Thread.sleep(5000);
					
				}
				
			}
			
		}
		
	}

	private void kishoreAccWeapons() throws InterruptedException {
		synchronized(weapon3)
		{
			System.out.println(" kishore has accuired "+weapon3);
			Thread.sleep(5000);
			synchronized(weapon2)
			{
				System.out.println(" kishore has accuired "+weapon2);
				Thread.sleep(5000);
				synchronized(weapon1)
				{
					System.out.println(" kishore has accuired "+weapon1);
					Thread.sleep(5000);
					
				}
				
			}
			
		}
		
		
	}
	
	
}

public class Demo6 
{
  public static void main(String[] args)
  {
	  CounterStrike t1 = new CounterStrike();
	  CounterStrike t2= new CounterStrike();
	  t1.setName("raunaq");
	  t2.setName("kishore");
	  t1.start();
	  t2.start();
	  
	
	  
  }
}

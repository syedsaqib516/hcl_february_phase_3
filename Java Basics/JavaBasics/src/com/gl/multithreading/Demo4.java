package com.gl.multithreading;


class WordDoc extends Thread
{
	public void run()
	{
		if(getName().equals("typing"))
		{
			typing();
		}
		else if(getName().equals("autoCorrect"))
		{
			autoCorrect();
		}
		else
		{
			autoSave();
		}
	}

	private void autoSave() {
		
		for (;;) {
			System.out.println("autoSaving...");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void autoCorrect() {
		for (;;) {
			System.out.println("autoCorrecting...");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	private void typing() {
		
		for (int i = 0; i < 5; i++) {
			System.out.println("typing...");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}
	
}

public class Demo4 
{
	public static void main(String[] args)
	{
		WordDoc t1=new WordDoc();
		WordDoc t2= new WordDoc();
		WordDoc t3=new WordDoc();
		t1.setName("typing");
		t2.setName("autoCorrect");
		t3.setName("autoSave");
		t2.setDaemon(true);
		t3.setDaemon(true);
		t2.setPriority(1);
		t3.setPriority(1);
		t1.setPriority(10);
		t1.start();
		t2.start();
		t3.start();		
	}

}

package com.gl.basics;

public class MemebersMain 
{
	public static void main(String[] args) 
	{
		//accessing non static members
		MembersOfClass	refVar  = new MembersOfClass(10);
		refVar.method1();
		System.out.println(refVar.x);
		refVar.x=100;
		
		MembersOfClass	refVar2  = new MembersOfClass(100,10);
		refVar2.x=200;
		
		System.out.println(refVar.x);//100  100  200  10
		System.out.println(refVar2.x);//100  200 200  10
		
		MembersOfClass	refVar3  = new MembersOfClass(1000,20.11);
		System.out.println(refVar3.x);// 0 , 10, 100 , 200, 
		
		
		// accessing static members
		MembersOfClass.y=10.10;
		MembersOfClass.method2();
		
	}

}

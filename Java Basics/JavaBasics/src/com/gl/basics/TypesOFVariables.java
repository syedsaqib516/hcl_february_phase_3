package com.gl.basics;

public class TypesOFVariables 
{
	
	int x =10;                      // instance variables / non static variables
	
	static double y=20.11;          //  class variable / static varaibles
	
	void m1()
	{
		// static int a=10;        //    CTE
	}
	
	static void m2(int x)      // local var
	{
		 int a=10;              // local var and local var are not static or non static
		//static int a=10;         //     CTE
	}
	

}

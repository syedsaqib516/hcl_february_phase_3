package com.gl.basics;

public class ConditionalStatement 
{
	public static void main(String[] args)
	{
		int age = 60;
		
		if(age>=18)
		{
			System.out.println(" eligible to vote");
			
		}
		else if(age>=70)
		{
			System.out.println("too old to vote");
		}
		else
		{
			System.out.println("not eligible to vote");
		}
		
		switch(age) {
		
		case 18: System.out.println("eligible to vote");
		break;
		case 60: System.out.println("too old to vote");
		break;
		case 120: System.out.println("probably dead");
		break;
		default: System.out.println("too young");
		
		}		
	}

}

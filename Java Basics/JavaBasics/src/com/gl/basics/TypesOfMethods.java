package com.gl.basics;

public class TypesOfMethods 
{
	
	public void welcome()
	{
		System.out.println("welcome to my calculator");
	}
	
	static void  add(int num1,int num2)
	{
		System.out.println("sum of num1 and num2 = "+(num1+num2));
	}
	static void add(int num1,int num2,int num3)
	{
		System.out.println("sum of num1 and num2 = "+(num1+num2));
	}
	double add(int num1, double num2)
	{
		double res= num1*num2;
		return res;
	}
	
	double mul(int num1, double num2)
	{
		double res= num1*num2;
		return res;
	}
	Double div(Double num1,Double num2) 
     {
		 return num1/num2;
			
	 }
	
	
	
	
	public static void main(String[] args)
	{
		TypesOfMethods x1=	new TypesOfMethods(); 
		x1.welcome();
		
		int x=10;
		int y=20;
		x1.add(x, y);
		
		double prod=x1.mul(100, 200.25);
		System.out.println(prod);
		
		Double quot=x1.div(100.0,50.9);
		System.out.println(quot);
		
		
		
	}

	

}

package com.gl.basics;


public class Customer // java bean or pojo class
{
	private String custId;
	private String custName;
	private String custEmail;
	
	public Customer(String custId, String custName, String custEmail) {
		super();
		this.custId = custId;
		this.custName = custName;
		this.custEmail = custEmail;
	}

	public String getCustId()
	{
		return custId;
	}
	
	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustEmail() {
		return custEmail;
	}

	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}

	@Override
	public String toString() {
		return "Customer [custId=" + custId + ", custName=" + custName + ", custEmail=" + custEmail + "]";
	}

	public void setCustId(String custId)
	{
		this.custId=custId;
	}
	
	

	
	
}

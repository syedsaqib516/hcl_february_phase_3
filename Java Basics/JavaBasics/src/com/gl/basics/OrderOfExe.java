package com.gl.basics;

public class OrderOfExe
{
	static int x=10;
	int y=20;
	
	static void m1()

	{
		System.out.println("static method");
	}
	
	void m2() {
		System.out.println("non static method");
	}
	
	static{
		System.out.println("static block");
	}
	
	{
		System.out.println("non static block");
	}
	OrderOfExe()
	{
		System.out.println("constructor");
	}
	public static void main(String[] args) 
	{
		System.out.println("main start");
		System.out.println("static "+x);
		m1();
		OrderOfExe o=new OrderOfExe();
		System.out.println("non static "+o.y);
		o.m2();
		System.out.println("main end");
		
	}
	
}

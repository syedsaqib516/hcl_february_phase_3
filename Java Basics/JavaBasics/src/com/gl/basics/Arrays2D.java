package com.gl.basics;

import java.util.Scanner;

public class Arrays2D
{
	public static void main(String[] args) 
	{

		int[][] marks= new int[1][1];
//		marks[0]=new int[3];
//		marks[1]=new int[5];

		Scanner scan = new Scanner(System.in);

		for (int i = 0; i < marks.length; i++) {
			for (int j = 0; j < marks[i].length; j++) {
				System.out.println("enter marks of class "+i+ " student "+j);
				marks[i][j]=scan.nextInt();
			}
		}
		
		for (int i = 0; i < marks.length; i++) {
			for (int j = 0; j < marks[i].length; j++) {
				System.out.print("enter marks of class "+i+ " student "+j+ "  ");
				System.out.print(marks[i][j]);
			}
			System.out.println();
		}


	}

}

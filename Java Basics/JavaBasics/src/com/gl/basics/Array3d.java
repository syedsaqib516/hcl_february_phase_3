package com.gl.basics;

import java.util.Scanner;

public class Array3d {
	public static void main(String[] args) {

		int[][][] marks = new int[2][][];
		marks[0]=new int[3][];
		marks[0][0]=new int[3];
		marks[0][1]=new int[3];
		marks[0][2]=new int[1];

		marks[1]=new int[2][];
		marks[1][0]=new int[5];
		marks[1][1]=new int[3];
		
		Scanner scan = new Scanner(System.in);
		
		for (int i = 0; i < marks.length; i++) {
			for (int j = 0; j < marks[i].length; j++) {
				for (int k = 0; k < marks[i][j].length; k++) {
				System.out.println("enter marks of school "+i+ " class "+j+ " student "+k);
				marks[i][j][k]=scan.nextInt();
			}
		}
	}

		for (int i = 0; i < marks.length; i++) {
			for (int j = 0; j < marks[i].length; j++) {
				for (int k = 0; k < marks[i][j].length; k++) {
				System.out.print("enter marks of school "+i+ " class "+j+ " student "+k);
				System.out.print(marks[i][j][k]);
			}
				System.out.println();
		}
	}


	}

}

package com.gl.basics;

public class StringBasic 
{
	public static void main(String[] args) 
	{
		//immutable strings
		String s1= new String("Syed");
		String s2= "Saqib";
		
		s1=s1.concat(s2);
		System.out.println(s1);
		
		
		//mutable string
		StringBuffer sb = new StringBuffer("Syed");
		sb.append("Saqib");
		System.out.println(sb);
		
		
		
		String x= "abc";
		x.toUpperCase(); //ABC
		String y=x.concat("XYZ");//abcXYZ
		System.out.println(y);// ABCXYZ , abcXYZ , ABCxyz ,aBCxYz
		
		
		char c=x.charAt(0);
		System.out.println(x.codePointAt(0));	
		
		String name= "rajarammohanroy";
		System.out.println(name.contains("raja"));
		System.out.println(name.substring(7));
		System.out.println(name.substring(7, 12));
		
		
		
		String message = "Sfoef{wpvt!bu!8!bn!jo!ubk!ipufm";
		int encryptKey=1;
		char[] chars =message.toCharArray();
		
		for(char char1: chars)// for each loop or enhances for loop
		{
			char1=(char)(char1-encryptKey);
			System.out.print(char1);
		}
		
		final float PI=3.14f;
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
}

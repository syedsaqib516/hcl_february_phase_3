package com.gl.basics;

import java.util.Scanner;

public class ReadingUseInput 
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);

		System.out.println("Read a user String");
		String s=  scan.next();
		System.out.println(s);
		scan.nextLine();
		System.out.println("Read a user sentence");
		String s1= scan.nextLine();
		System.out.println(s1);
		System.out.println("enter a byte");
		byte b=    scan.nextByte();
		System.out.println(b);
		System.out.println("enter a short");
		short st=   scan.nextShort();
		System.out.println(st);
		System.out.println("enter a int");
		int i=  scan.nextInt();
		System.out.println(i);
		System.out.println("enter long");
		long l =  scan.nextLong();
		System.out.println(l);
		System.out.println("enter a float");
		float f=    scan.nextFloat();
		System.out.println(f);
		System.out.println("enter a double");
		double d=    scan.nextDouble();
		System.out.println(d);
		System.out.println("enter a boolean");
		boolean bool=     scan.nextBoolean();
		System.out.println(bool);
		System.out.println("enter a char");
		char c=     scan.next().charAt(0);
		System.out.println(c);


	}

}

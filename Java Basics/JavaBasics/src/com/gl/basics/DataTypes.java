package com.gl.basics;

public class DataTypes 
{
	
	public static void main(String[] args) {
		byte nameOfData = 127;
		System.out.println(++nameOfData);
		
		byte a=127;
		short b=a;// implict type casting
		
		int x= 127;
		a=(byte)x;// explicit type casting
		
		float f= 123.23f;
		x=(int)f; 
		System.out.println(x);
		
		
	}
}

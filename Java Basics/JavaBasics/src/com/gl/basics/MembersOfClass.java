package com.gl.basics;

public class MembersOfClass 
{
		
    int x=10;
    
    static double y = 20.11;
    
    
    void method1()
    {
    	System.out.println("non static method");
    	
    }
    
    static void method2()
    {
    	System.out.println("static method");
    	
    }
    
    MembersOfClass(int x)
    {
    	System.out.println("hi from int ");
    }
    MembersOfClass(int x,int y)
    {
    	System.out.println("hi from int int constructor");
    }
    MembersOfClass(int x,double z)
    {
    	System.out.println("hi from inr double constructor");
    }
     
}

package com.gl.collections;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

class MyComparator1 implements Comparator<Integer>
{
	
	@Override
	public int compare(Integer arg0, Integer arg1) {
		
		return -arg0.compareTo(arg1);
	}
	
}

public class HashSetDemo 
{
	public static void main(String[] args) {
		ArrayList al = new ArrayList();
		al.add(10);
		al.add(20);
		al.add(30);
		al.add(40);
		al.add(50);
		al.add(50);
		al.add(60);
		al.add(70);
		al.add(80);
		al.add(90);
		
		TreeSet<Integer> hs = new TreeSet<Integer>((I1,I2)-> -I1.compareTo(I2));// 16 , 0.75 (LinkedList + hashtable)
		hs.add(31);
		hs.add(12);
		hs.add(23);
		hs.add(42);
		hs.add(15);
		hs.add(66);
		System.out.println(hs);
		
		// 12.compareTo(31); -1 0 +1
		// 23.compareTo(31);
		// 23.compareTo()12; 
		
		
		
		
		
		
		
		
		
	}

}

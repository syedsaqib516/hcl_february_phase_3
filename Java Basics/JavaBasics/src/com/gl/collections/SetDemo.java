package com.gl.collections;

import java.util.TreeSet;

public class SetDemo 
{
	public static void main(String[] args) {
//		HashSet s = new HashSet();
//	    s.add(10);
//	    s.add(20);
//	    s.add(30);
//	    s.add(40);
//	    s.add(50);
//	    s.add(30);
//	    s.add(50);
//	    System.out.println(s);
//	    LinkedHashSet s = new LinkedHashSet();
//	    s.add(10);
//	    s.add(20);
//	    s.add(30);
//	    s.add(40);
//	    s.add(50);
//	    s.add(30);
//	    s.add(50);
//	    System.out.println(s);
	    
	    TreeSet<Integer> s = new TreeSet<Integer>(new MyComparator());	   
	    s.add(50);
	    s.add(30);
	    s.add(50);
	    s.add(10);
	    s.add(20);
	    s.add(30);
	    s.add(40);
	    System.out.println(s);
	    
	    
	}

}

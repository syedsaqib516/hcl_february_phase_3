package com.gl.collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;



public class MapsDemo 
{
	public static void main(String[] args) 
	{
		
		TreeMap<Integer,Employee> hme= new TreeMap<Integer,Employee>((I1,I2)-> -I1.compareTo(I2));
		
		hme.put(131, new Employee("abc","abc@gmail.com","Blr"));
		hme.put(123, new Employee("def","abc@gmail.com","Blr"));
		hme.put(154, new Employee("zyx","zyx@gmail.com","Blr"));
		
		System.out.println(hme);
		
		
		
		
		
		
		
		
		
		
		
		
				HashMap hm = new HashMap();
				hm.put(1, "abc");
				hm.put(2, 2);
				hm.put(3, 'c');
				hm.put(null, null);
			    hm.put(null, 'w');
				System.out.println(hm);

		LinkedHashMap hm1 = new LinkedHashMap();
		hm1.put(1, "abc");
		hm1.put(2, 2);
		hm1.put(3, 'c');
		hm1.put(null, null);
		hm1.put(null, 'w');
		System.out.println(hm);

		TreeMap<Integer,String> tm = new TreeMap<Integer,String>(new MyComparator());
		tm.put(1, "abc");
		tm.put(3, "abcs");
		tm.put(2, "aabc");
		tm.put(5, "aasbc");
		tm.put(4, "abcccd");



		Set s1=hm.keySet();
		System.out.println(s1);
		System.out.println(hm.values());

		System.out.println(tm);

		Set<Entry<Integer,String>> s	=tm.entrySet();

		Iterator<Entry<Integer,String>> itr= s.iterator();

		while(itr.hasNext())
		{
			Entry<Integer,String> e= itr.next();
			System.out.println(e.getKey()+"  "+e.getValue());
		}



	}

}

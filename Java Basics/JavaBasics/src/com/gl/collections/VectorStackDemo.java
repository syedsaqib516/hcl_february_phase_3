package com.gl.collections;

import java.util.Enumeration;
import java.util.Stack;
import java.util.Vector;

public class VectorStackDemo 
{
	public static void main(String[] args) {
		Vector al = new Vector();
		al.add(10);
		al.add(10);
		al.add(10);
		al.add(10);
		al.addElement(10);
		Enumeration en= al.elements();
		while(en.hasMoreElements())
		{
			System.out.println(en.nextElement());
		}
	//	System.out.println(al);
		
		Stack s = new Stack();
		
		  s.push(10);
		  s.push(20);
		  s.push(30);
		  System.out.println(s);
		  s.pop();
		  System.out.println(s);
		  System.out.println(s.peek());
		 
	}
	

}

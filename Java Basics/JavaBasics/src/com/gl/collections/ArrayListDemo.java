package com.gl.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class ArrayListDemo 
{
	public static void main(String[] args) 
	{
		ArrayList al = new ArrayList();
		al.add(10);
		al.add(20);
		al.add(30);
		al.add(40);
		al.add(50);
		al.add(50);
		al.add(60);
		al.add(70);
		al.add(80);
		al.add(90);
		System.out.println(al);
		
		System.out.println("for loop");
		for(int i =0 ; i<al.size(); i++)
		{
			System.out.println(al.get(i));
		}
		System.out.println("for each");
		for(Object i: al)
		{
			System.out.println(i);
		}
		System.out.println("iterator");
		Iterator itr=al.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		ListIterator litr=al.listIterator();
		
		System.out.println("list iterator");
		while(litr.hasNext())
		{
			System.out.println(litr.next());
		}
		
		System.out.println("descending iterator");
		
		
		ListIterator ditr=al.listIterator(al.size());
		System.out.println("list iterator");
		
		while(ditr.hasPrevious())
		{
			System.out.println(ditr.previous());
		}
		
		
		
	}

}

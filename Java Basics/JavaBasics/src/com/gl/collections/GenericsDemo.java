package com.gl.collections;

class Generic <T>
{
	T x;

	public Generic(T x) {
		if (!(x instanceof String || x instanceof Character)) {
			throw new IllegalArgumentException("Only String and Character types are allowed.");
		}
		this.x = x;
	}

	public T getX() {
		return x;
	}
	public void setX(T x) {
		this.x = x;
	}

}
public class GenericsDemo
{
	public static void main(String[] args) 
	{
		Generic<String> g1=new Generic<String>("s");
		System.out.println(g1.getX());
		Generic<Character> g2=new Generic<Character>('a');
		System.out.println(g2.getX());
		
		Object[] obj = new Object[5];
		obj[1]= "abc";
		obj[2]=10;
		obj[3]=10.2f;
		obj[4]=new Generic("s");
		
		
		
		
	}

}

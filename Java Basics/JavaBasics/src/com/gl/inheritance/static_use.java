package com.gl.inheritance;

class access
{
	static int x;
	void increment()
	{
		x++;
	}
}
class static_use
{
	public static void main(String args[])
	{
		    String str = "Hello There!";
		    String mtr = "Hello" + " " + "There" + "!";
		    String dtr = "Hello " + "There" + "!";
		    String gtr = "Hello There" + "!";
		    System.out.println(str);
		    System.out.println(mtr);
		    System.out.println(gtr);
		    System.out.println(dtr);
		    
		    System.out.println(System.identityHashCode(gtr));
		    System.out.println(System.identityHashCode(str));
		    System.out.println(System.identityHashCode(mtr));
		    System.out.println(System.identityHashCode(dtr));
	}
}

package com.gl.inheritance;

import java.util.Objects;

class Test
{
	int x=10;

	@Override
	public String toString() {
		return "Test [x=" + x + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(x);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Test other = (Test) obj;
		return x == other.x;
	}
	
}
public class Demo 
{
	public static void main(String[] args) 
	{
       Object obj1= new Object();
       Object obj2 = new Object();
       System.out.println(obj1.hashCode());
       System.out.println(obj1.getClass().getName());
       System.out.println(obj1.equals(obj2));
       
      System.out.println( obj1.toString());
      
      String s=new String("abcd");
      String s1=new String("abcd");
      System.out.println(s1.equals(s));
      System.out.println(s);
      
      System.out.println(s.toString());
      
      Test t = new Test();
      Test t1 = new Test();
      System.out.println(t.equals(t1));
      System.out.println(t.getClass().getName());
      
      
       
	}	

}

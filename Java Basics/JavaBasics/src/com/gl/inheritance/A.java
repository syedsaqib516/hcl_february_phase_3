package com.gl.inheritance;

public class A {
	public int x;
	int y;
	protected int z;
	static int a;
	
	void m1()
	{
		System.out.println("non static method");
	}
	static void m2()
	{
		System.out.println("static method");
	}
}

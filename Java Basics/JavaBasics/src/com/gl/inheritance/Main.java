package com.gl.inheritance;

//class Object
//{
//	Object ()
//	{
//		// empty
//	}
//}



class X extends Object
{
	final int var=10;
	X(int x)
	{
		//super();      implicit constructor chainng
		System.out.println("X constructor");
	}
//	X()
//	{
//		//super();      implicit constructor chainng
//		System.out.println("X constructor");
//	}
	
	
}
class Y extends X
{
	int var=100;
	Y(int x)
	{
		super(10);// explicit constructor chainng
		System.out.println("Y constructor");
		System.out.println(var);
		System.out.println(super.var);
	}

//	Y()
//	{
//		//super(); implicit constructor chainng
//		System.out.println("Y constructor");
//	}
	
	
	
}



public class Main
{
	public static void main(String[] args)
	{
	System.out.println("main start");
	   new Y(10);
	System.out.println("main end");
		
	}

}
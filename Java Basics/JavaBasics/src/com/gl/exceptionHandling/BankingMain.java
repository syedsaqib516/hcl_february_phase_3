package com.gl.exceptionHandling;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class BankingMain 
{
	public static void main(String[] args) throws FileNotFoundException, IOException {
		System.out.println("welcome to Great Learning Bank");
		BankTransaction bt = new BankTransaction();
		bt.validateBalance(10000, 40000);		
		System.out.println("Thank You, Continue banking with us");
		
		String line;
		   try(BufferedReader br = new BufferedReader(new FileReader("test.txt"))) {
			      while ((line = br.readLine()) != null) {
			        System.out.println("Line =>"+line);
			      }
			    } 
	  
	}

}

package com.gl.exceptionHandling;



public class Demo2 
{
	public static void main(String[] args) 
	{
		System.out.println("main start");
		try {
			m1();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("main end");
	}

	private static void m1() {
		System.out.println("m1 start");
		m2();
		System.out.println("m1 end");

	}

	private static void m2() {
		System.out.println("m2 start");
		m3();

		System.out.println("m2 end");
	}

	private static void m3() {
		System.out.println("m3 start");
		System.out.println(10/0);
		System.out.println("m3 end");

	}



}

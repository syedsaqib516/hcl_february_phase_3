package com.gl.exceptionHandling;

import java.io.IOException;

public class Demo3 
{
	public static void main(String[] args) throws Exception 
	{
		System.out.println("main start");		
		m1();
		System.out.println("main end");
	}

	private static void m1() throws Exception {
		System.out.println("m1 start");
		m2();
		System.out.println("m1 end");

	}

	private static void m2() throws IOException,Exception {
		System.out.println("m2 start");
		m3();
		System.out.println("m2 end");
	}

	private static void m3() throws IOException {
		System.out.println("m3 start");
		throw new IOException();
		
	}



}

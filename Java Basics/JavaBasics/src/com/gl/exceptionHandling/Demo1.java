package com.gl.exceptionHandling;

import java.util.InputMismatchException;

public class Demo1 
{
	public static void main(String[] args) {
		System.out.println("main start");
		
		try {
			System.out.println(10/0);// suspicious line of code/ where user input is taken Execption object created 
		}		
		catch( ArithmeticException | InputMismatchException ae)// specific
		{
			
			System.out.println("please enter valid input");
		}
		catch(Exception ae)// generic
		{
			System.out.println("something went worng try again");
		}
		
		finally
		{
			System.out.println("finally executed");
			// will execute irrespective of an excption occuring in the try block
			// writing clean up activity
			// like closing the resources (an openened datbase or a file)
			// finally only executes when control reaches the try block
		}
				
		System.out.println("main end");
	}

}

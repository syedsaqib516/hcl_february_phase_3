package com.gl.exceptionHandling;

public class BankTransaction
{
    public void validateBalance(double amtBal,double amtWithdraw)
    {
       System.out.println("validation started");
       
       if(amtBal>amtWithdraw)
       {
    	   System.out.println("withdrawl successful");
    	   System.out.println("Please Collect your cash");
    	   amtBal-=amtWithdraw;
       }
       else
       {
    	   try {
    		   throw new InsufficientBalanceException("you do not have sufficient balance");
    	   }
    	   catch(RuntimeException re)
    	   {
    		  System.out.println(re.getMessage());
    	   }
       }
       
       System.out.println("avail balance = "+amtBal);
       System.out.println("validation ended");
    }
}



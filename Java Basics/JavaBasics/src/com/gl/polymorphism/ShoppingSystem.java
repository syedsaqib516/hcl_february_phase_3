package com.gl.polymorphism;

import java.util.Date;
import java.util.Scanner;

class Customer {
	private int customerId;
	private String customerName;
	private String email;

	public Customer(int customerId, String customerName, String email) {
		this.customerId = customerId;
		this.customerName = customerName;
		this.email = email;
	}

	public int getCustomerId() {
		return customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public String getEmail() {
		return email;
	}
}


class Product {
	private int productId;
	private String productName;
	private double price;

	public Product(int productId, String productName, double price) {
		this.productId = productId;
		this.productName = productName;
		this.price = price;
	}

	public int getProductId() {
		return productId;
	}

	public String getProductName() {
		return productName;
	}

	public double getPrice() {
		return price;
	}
}








class Order {
	private int orderId;
	private Customer customer;
	private Product[] products;
	private Date orderDate;

	public Order(int orderId, Customer customer, Date orderDate, int maxSize) {
		this.orderId = orderId;
		this.customer = customer;
		this.products = new Product[maxSize];
		this.orderDate = orderDate;
	}

	public int getOrderId() {
		return orderId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public Product[] getProducts() {
		return products;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void addProduct(Product product) {
		for (int i = 0; i < products.length; i++) {
			if (products[i] == null) {
				products[i] = product;
				break;
			}
		}
	}

	public double calculateTotal() {

		// learners need to calculate
		double total = 0;
		for (Product product : products) {
			if (product != null) {
				total += product.getPrice();
			}
		}
		return total;
	}
}

public class ShoppingSystem{
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int customerId=  scan.nextInt();

		String customerName= scan.nextLine();
		scan.nextLine();
		String email= scan.nextLine();

		int orderId=scan.nextInt();

		int maxSize=  scan.nextInt();

		int productId1=  scan.nextInt();
		
		String productName1= scan.nextLine();
		scan.nextLine();
		double price1=  scan.nextInt();

		int productId2=  scan.nextInt();

		String productName2= scan.nextLine();
		scan.nextLine();
		double price2=  scan.nextInt();



		Customer customer1 = new Customer(customerId,customerName, email);
		Order order = new Order(orderId, customer1, new Date(), maxSize);


		Product product1 = new Product(productId1,productName1, price1);
		Product product2 = new Product(productId2,productName2, price2);

		order.addProduct(product1);
		order.addProduct(product2);

		double expectedTotal = product1.getPrice() + product2.getPrice();
		double actualTotal = order.calculateTotal();
		System.out.println(expectedTotal);
		System.out.println(actualTotal);

	}
}

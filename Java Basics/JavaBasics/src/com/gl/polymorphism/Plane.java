package com.gl.polymorphism;

public class Plane 
{
    public void fly()
    {
    	System.out.println("fly");
    }
    public void carry()
    {
    	System.out.println("carry");
    }
}

class PassengerPlane extends Plane
{
    public void carry()
    {
    	System.out.println("carry passenger");
    }
    public void boardPassenger()
    {
    	System.out.println("boarding");
    }
}
class FighterPlane extends Plane
{
    public void carry()
    {
    	System.out.println("carry weapons");
    }
    public void activateMissile()
    {
    	System.out.println("target locked");
    }
}
class CargoPlane extends Plane
{
    public void carry()
    {
    	System.out.println("carry goods");
    }
    public void secureGoods()
    {
    	System.out.println("goods secured");
    }
}

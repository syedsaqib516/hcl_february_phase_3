package com.gl.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileIOStreamDemo 
{
	public static void main(String[] args) throws FileNotFoundException 
	{
		String inputFilePath = "input.txt";
		String outputFilePath = "output.txt";
		
		FileInputStream fis = new FileInputStream(inputFilePath);
		FileOutputStream fos = new FileOutputStream(outputFilePath);
		
		BufferedInputStream bis = new BufferedInputStream(fis);
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		
		int temp;
		
		try {
			while((temp=bis.read())!=-1)
			{		
				 
				 bos.write(temp);
				 bos.write(63);
			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		finally {
			try {
				//bos.flush();
				bis.close();
				bos.close();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
				
		
	}
	

}

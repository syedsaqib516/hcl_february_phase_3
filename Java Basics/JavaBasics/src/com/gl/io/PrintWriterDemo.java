package com.gl.io;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class PrintWriterDemo {
	public static void main(String[] args) throws FileNotFoundException {
		PrintWriter pw= new PrintWriter("demo.txt");
		pw.println(100);
		pw.println("Oh hello there");
		pw.println("hi");
		
		Car car1= new Car("Toyota","supra",192987);
		Car car2= new Car("Mahindra","Thar",1911987);
		Car car3= new Car("Nissan","GTR",190007);
		
		pw.println(car1);
		pw.println(car2);
		pw.println(car3);
		
		
		pw.flush();

	}
}

package com.gl.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReaderWriterDemo {
	public static void main(String[] args) throws IOException 
	{
		String inputFilePath = "input.txt";
		String outputFilePath = "output.txt";
		
		FileReader fr = new FileReader(inputFilePath);
		FileWriter fw = new FileWriter(outputFilePath);
		
		BufferedReader br = new BufferedReader(fr);
		BufferedWriter bw = new BufferedWriter(fw);
		
		String temp;
		
		try {
			while((temp=br.readLine())!=null)
			{		
				 
				 bw.write(temp);
				 bw.newLine();
			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		finally {
			try {
				//bw.flush();			
				br.close();
				bw.close();
				
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
				
		
	}
}

package com.gl.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializationDemo 
{
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		
		//Serialization
		Account acc= new Account(9090530015l,"salary","8090","SIBL0090");
		FileOutputStream fos = new FileOutputStream("accounts.txt");
		ObjectOutputStream oos= new ObjectOutputStream(fos);
		oos.writeObject(acc);
		
		//Deserialization
		FileInputStream fis = new FileInputStream("accounts.txt");
		ObjectInputStream ois= new ObjectInputStream(fis);
		Account accDes=(Account) ois.readObject();
		System.out.println(accDes);
	}

}

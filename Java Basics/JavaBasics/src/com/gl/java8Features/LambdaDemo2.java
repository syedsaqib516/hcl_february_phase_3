package com.gl.java8Features;

@FunctionalInterface
interface Itr1
{
	void add();	
}

@FunctionalInterface
interface Itr2
{
	int sum(int a,int b);
	
}

public class LambdaDemo2
{
	public static void main(String[] args) {
		Itr1 itr1 = ()-> System.out.println("add implemented");
		itr1.add();
		
		Itr2 itr2 =(a,b)-> a+b;
		System.out.println(itr2.sum(10, 20));		
	}

}

package com.gl.java8Features;
import java.util.*;
public class EkartMain
{
	
	public static Customer getCustByMail(String email) throws CustomerNotFoundException
	{
		List<Customer> customers=EkartDataBase.getAll();
		
		   return customers.stream()
		            .filter((customer)->customer.getCustEmail().equalsIgnoreCase(email))
		                .findFirst()
		                   .orElseThrow(()->new CustomerNotFoundException("No customer found with the given mail id"));
	}
	
	public static void main(String[] args) 
	{
	 
		
			Customer c=null;
			try {
				c = getCustByMail("saqib1@gmail.com");
			} catch (CustomerNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}
		   System.out.println(c);
		
	}

}

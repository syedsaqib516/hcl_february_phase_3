package com.gl.java8Features;

interface Itr3
{
	void m1();
	default public void newFeature()
	{
		System.out.println("new Feature");
	}
	
}
class A implements Itr3
{

	@Override
	public void m1() {
		System.out.println("m1");
		
	}
	@Override
	public void newFeature()
	{
		System.out.println("new Feature new impl");
	}
	
}
class B implements Itr3
{

	@Override
	public void m1() {
		System.out.println("m1");
		
	}
	
}
class C implements Itr3
{

	@Override
	public void m1() {
		System.out.println("m1");
		
	}
	
}



public class DefaultMethodsDemo {
	public static void main(String[] args) 
	{
		
	}

}

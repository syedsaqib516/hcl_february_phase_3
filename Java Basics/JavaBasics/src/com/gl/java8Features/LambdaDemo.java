package com.gl.java8Features;

//@FunctionalInterface
interface Itr
{
	void add();
	void add1();
}

public class LambdaDemo 
{
	public static void main(String[] args) {
		Itr itr = new Itr(){

			@Override
			public void add() {
				System.out.println("add implemented");
				
			}

			@Override
			public void add1() {
				System.out.println("add1 implemented");
				
			}
			
		};
		itr.add();
		itr.add1();
		
	}

}

package com.gl.java8Features;

import java.util.function.BiFunction;
import java.util.function.Function;

public class FunctionDemo 
{
	public static void main(String[] args) 
	{
      BiFunction<Integer,Integer,Integer> squareNum=(n,m)->n*m;
      Function<Integer,Integer> doubleNum=n->2*n;
      Function<Integer,Integer> cubeNum=n->n*n*n;
      
      System.out.println(squareNum.apply(4,5));
      
      cubeNum.andThen(doubleNum).apply(2);//4  4  16  64
      doubleNum.andThen(cubeNum).apply(2);//4  8  64  16
      
      System.out.println(cubeNum.andThen(doubleNum).apply(2));
      System.out.println( doubleNum.andThen(cubeNum).apply(2));
    
      

	}
}

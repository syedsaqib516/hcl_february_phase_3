package com.gl.java8Features;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class StreamsDemo2 {
	public static void main(String[] args) {
		ArrayList<Integer> marks = new ArrayList<Integer>();
		marks.add(10);
		marks.add(0);
		marks.add(35);
		marks.add(20);
		marks.add(38);
		marks.add(70);
		marks.add(90);
		System.out.println(marks);

//		List<Integer> failedMarks=  marks.stream().filter(I->I<40).collect(Collectors.toList());
//		System.out.println(failedMarks);
//
//		long noOfFailedMarks=marks.stream().filter(I->I<40).count();
//		System.out.println(noOfFailedMarks);
//
//		List<Integer> newMarks=marks.stream().map(I->I+5).collect(Collectors.toList());
//		System.out.println(newMarks);
//
//		List<Integer> sortedMarksAsc=marks.stream().sorted().collect(Collectors.toList());
//		System.out.println(sortedMarksAsc);
//
//		List<Integer> sortedMarksDsc=marks.stream().sorted((i1,i2)->(i1<i2)?1:(i1>i2)?-1:0).collect(Collectors.toList());
//		System.out.println(sortedMarksDsc);
//
//		List<Integer> sortedMarksDsc1=marks.stream().sorted((i1,i2)->i2.compareTo(i1)).collect(Collectors.toList());
//		System.out.println(sortedMarksDsc1);
//
//		Integer minMarks=marks.stream().min((i1,i2)->i1.compareTo(i2)).get();
//		System.out.println(minMarks);
//
//		Optional maxMarks=marks.stream().max((i1,i2)->i1.compareTo(i2));
//		System.out.println(maxMarks.get());
//		
		
		marks.stream().forEach(System.out::println);


		Consumer<Integer> c= i->{
			System.out.println(" the square of the marks "+i+" is = "+(i*i));
		};

		marks.stream().forEach(c);
		marks.stream().forEach(i->{
			System.out.println(" the square of the marks "+i+" is = "+(i*i));
		});

		Object[] marksArr=marks.stream().toArray();
		for(Object obj1:marksArr)
		{
			System.out.println(obj1);
		}

		Integer[] arrMarks=  marks.stream().toArray(Integer[]::new);
		for(Integer i1:arrMarks)
		{
			System.out.println(i1);
		}



	}
}

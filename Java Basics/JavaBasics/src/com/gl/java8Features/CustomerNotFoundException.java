package com.gl.java8Features;

public class CustomerNotFoundException extends Exception {
	CustomerNotFoundException(String message)
	{
		super(message);
	}

}

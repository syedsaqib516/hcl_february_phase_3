package com.gl.java8Features;

interface Itr4
{
	static void m1()
	{
		System.out.println("static method of interface");
	}
}

public class StaticMethodJava8 implements Itr4
{
	public static void main(String[] args) 
	
	{
		Itr4.m1();
//		m1();
//		 StaticMethodJava8 s= new StaticMethodJava8();
//		 s.m1();
//		StaticMethodJava8.m1();
		
	}

}

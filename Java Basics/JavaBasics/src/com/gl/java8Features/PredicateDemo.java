package com.gl.java8Features;

import java.util.function.Predicate;

public class PredicateDemo {
	public static void main(String[] args) {
		Predicate<Integer> p = I->I%2==0;
		
		System.out.println(p.test(10));
		System.out.println(p.test(11));
		
		if(p.test(10))
		{
			System.out.println("number is even");
		}
		else
		{
			System.out.println("odd");
		}
		
		String[] names = {"saqib","hemashri","mayuri","samrudhi","pooja","shri","hari"}; 
		
		Predicate<String> p1 =s-> s.length()>5;
		Predicate<String> p2 =s-> s.length()%2==0;
		
		for(String name:names)
		{
			if(p1.and(p2).test(name))
			{
				System.out.println(name);
			}
//			if(p1.or(p2).test(name))
//			{
//				System.out.println(name);
//			}
//			if(p2.negate().test(name))
//			{
//				System.out.println(name);
//			}
		}
		
		
		
	}

}

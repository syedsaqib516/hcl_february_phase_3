package com.gl.java8Features;

import java.util.Optional;

public class OptionalClass 
{
	public static void main(String[] args) 
	{
		Optional o= Optional.empty();
		System.out.println(o);
		Optional o1=Optional.of("saqib");
		System.out.println(o1);
		Optional o2= Optional.ofNullable(null);
		System.out.println(o2);
		 String s=(String) o1.get();

		System.out.println(s);
		
		if(o1.isPresent())
		{
			System.out.println("object is present ");
			
		}
		System.out.println(o2.orElseGet(()->"saqib@gmail"));
		try {
			o2.orElseThrow(()-> new Exception("inavlid user"));
		} catch (Throwable e) {
			System.out.println("execption handled "+e.getMessage());
		}
	}

}

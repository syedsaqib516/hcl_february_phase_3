package com.gl.java8Features;

class Singleton
{
	private static Singleton obj=null;

	// private constructor to force use of
	// getInstance() to create Singleton object
	private Singleton() {}

	public static Singleton getInstance()// factory method
	{
		if (obj==null)
			obj = new Singleton();
		return obj;
	}
}

class Singleton1
{
	private static Singleton1 obj;

	private Singleton1() {}

	// Only one thread can execute this at a time
	public static synchronized Singleton1 getInstance()
	{
		if (obj==null)
			obj = new Singleton1();
		return obj;
	}
}

class Singleton2
{
	private static Singleton2 obj = new Singleton2();

	private Singleton2() {}

	public static Singleton2 getInstance()
	{
		return obj;
	}
}
class Singleton3
{
	private static Singleton3 obj;
	static {
		obj = new Singleton3();
	}

	private Singleton3() {}

	public static Singleton3 getInstance()
	{
		return obj;
	}
}
public class SingletonDemo
{
	public static void main(String[] args) {
		Singleton s = Singleton.getInstance();
		System.out.println(s.hashCode());
		Singleton s1 = Singleton.getInstance();
		System.out.println(s.hashCode());
		Singleton s2 = Singleton.getInstance();
		System.out.println(s.hashCode());
	}
}

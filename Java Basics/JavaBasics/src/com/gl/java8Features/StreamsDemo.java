package com.gl.java8Features;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StreamsDemo 
{
	public static void main(String[] args) 
	{
		ArrayList<Integer> marks = new ArrayList<Integer>();
		marks.add(10);
		marks.add(20);
		marks.add(70);
		marks.add(50);
		marks.add(60);
		marks.add(100);
		marks.add(90);
		
		List failedMarks = new ArrayList();
//		Predicate<Integer> p =I-> I < 60;
		failedMarks= (List) marks.stream().filter(I->I<60).collect(Collectors.toList());
		
		
		System.out.println(failedMarks);
	}

}

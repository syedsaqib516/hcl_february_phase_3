package com.gl.java8Features;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class DateTimeAPI 
{
	public static void main(String[] args) 
	{
		LocalDate ld= LocalDate.now();
		System.out.println(ld);
		LocalTime lt = LocalTime.now();
		System.out.println(lt);
		LocalDateTime ldt= LocalDateTime.now();
		System.out.println(ldt);
		
		LocalDate bday1=	LocalDate.of(1996, 4, 19);
		
		LocalDate bday2=	LocalDate.of(1996, Month.APRIL, 19);
		LocalTime btime= LocalTime.of(7,0,0,0);
		
	   System.out.println(bday1.getDayOfWeek());
	   
	   Period p = Period.between(bday1, ld);
	   System.out.println(p);
	   System.out.println(p.getYears());
	   System.out.println(p.getMonths());
	   
	   
	   ZoneId zi = ZoneId.of("America/Chicago");
	   ZonedDateTime zdt = ZonedDateTime.now(zi);
	   System.out.println(zdt);
	   
		
		
		
		
		
		
	}

}

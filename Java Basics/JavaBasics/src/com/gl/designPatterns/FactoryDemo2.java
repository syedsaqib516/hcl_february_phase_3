package com.gl.designPatterns;

class Test
{
	int n;
	
	private Test(int n)
	{
		this.n=n;
	}
	
	@Override
	public String toString() {
		return "Test [n=" + n + "]";
	}
	
	public static Test getTest(int n)// factory method
	{
		return new Test(n); 
	}
}

public class FactoryDemo2 
{
	public static void main(String[] args) {
		Test t = Test.getTest(10);
		System.out.println(t);
		
	}

}

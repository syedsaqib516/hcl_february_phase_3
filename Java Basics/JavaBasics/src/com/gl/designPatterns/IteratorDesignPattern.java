package com.gl.designPatterns;

interface Iterator {
	   public boolean hasNext();
	   public Object next();
	}

interface Container {
	   public Iterator getIterator();
	}

class NameRepository implements Container {
	   public String names[] = {"Robert" , "John" ,"Julie" , "Lora"};

	   @Override
	   public Iterator getIterator() {
	      return new NameIterator();
	   }

	   private class NameIterator implements Iterator {

	      int index;//0,1,2,3,4

	      @Override
	      public boolean hasNext() {
	      
	         if(index < names.length){
	            return true;
	         }
	         return false;
	      }

	      @Override
	      public Object next() {
	      
	         if(this.hasNext()){
	            return names[index++];
	         }
	         return null;
	      }		
	   }
	}

public class IteratorDesignPattern 
{
	public static void main(String[] args) {
	      NameRepository namesRepository = new NameRepository();

	      for(Iterator iter = namesRepository.getIterator(); iter.hasNext();){
	         String name = (String)iter.next();
	         System.out.println("Name : " + name);
	      }
	         
	         Iterator iter1 = namesRepository.getIterator();   
	         while(iter1.hasNext())
	         {
	        	  String name1 = (String)iter1.next();
	 	         System.out.println("Name1 : " + name1);
	         }
	      	
	   }

}

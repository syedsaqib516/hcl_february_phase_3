package com.gl.designPatterns;

class Student implements Cloneable
{
	private int roll;
	private String name;
	
	public Student(int roll, String name) {
		super();
		this.roll = roll;
		this.name = name;
	}
	@Override
	public String toString() {
		return "Student [roll=" + roll + ", name=" + name + "]";
	}
	public int getRoll() {
		return roll;
	}
	public void setRoll(int roll) {
		this.roll = roll;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
	
}
public class PrototypeDemo {
	public static void main(String[] args) throws CloneNotSupportedException
	{
		Student s= new Student(1,"abcd");
		System.out.println(s);
		
		Student s1=(Student) s.clone();
		System.out.println(s1);
		
	}

}


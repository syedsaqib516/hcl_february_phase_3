package com.gl.designPatterns;

interface Movable {
	// returns speed in MPH 
	double getSpeed();
}
class BugattiVeyron implements Movable {

	@Override
	public double getSpeed() {
		return 268;
	}
}	
class Ferrari488 implements Movable {

	@Override
	public double getSpeed() {
		return 189;
	}
}
class AstonMartinDB11 implements Movable {

	@Override
	public double getSpeed() {
		return 182;
	}
}


interface MovableAdapter {
	// returns speed in KM/H 
	double getSpeed();
}

class MovableAdapterImpl implements MovableAdapter {
	private Movable luxuryCars;

	// standard constructors

	public MovableAdapterImpl(Movable luxuryCars) {
		this.luxuryCars=luxuryCars;
	}

	@Override
	public double getSpeed() {
		return convertMPHtoKMPH(luxuryCars.getSpeed());
	}

	private double convertMPHtoKMPH(double mph) {
		return mph * 1.60934;
	}
}

public class AdapterDemo
{
	

	public static void main(String[] args) {
		Movable bugattiVeyron = new BugattiVeyron();
		System.out.println( bugattiVeyron.getSpeed());
		MovableAdapter bugattiVeyronAdapter = new MovableAdapterImpl(bugattiVeyron);
		System.out.println(bugattiVeyronAdapter.getSpeed());

	}
}

package com.gl.designPatterns;

public class Main {
	public static void main(String[] args) {
		SingletonDemo s1=	SingletonDemo.getSingletonDemo();
		SingletonDemo s2=   SingletonDemo.getSingletonDemo();
		
		System.out.println(System.identityHashCode(s1));
		System.out.println(System.identityHashCode(s2));
	}
}

package com.gl.designPatterns;

public class SingletonDemo
{
	private static SingletonDemo s=null;
	
	private SingletonDemo()
	{
		
	}
	
	public static SingletonDemo getSingletonDemo()
	{	
		if(s==null) {
		s= new SingletonDemo();
		}
		return s;
	}
	
}

package com.gl.abstraction;


abstract public class Plane 
{
	abstract  public void fly();
	abstract public void carry();
}

class PassengerPlane extends Plane
{
    public void carry()
    {
    	System.out.println("carry passenger");
    }
    public void boardPassenger()
    {
    	System.out.println("boarding");
    }
	@Override
	public void fly() {
		System.out.println("flies at med altitude");
		
	}
}
class FighterPlane extends Plane
{
    public void carry()
    {
    	System.out.println("carry weapons");
    }
    public void activateMissile()
    {
    	System.out.println("target locked");
    }
	@Override
	public void fly() {
		System.out.println("flies at high altitiude");
		
	}
}
class CargoPlane extends Plane
{
    public void carry()
    {
    	System.out.println("carry goods");
    }
    public void secureGoods()
    {
    	System.out.println("goods secured");
    }
	@Override
	public void fly() {
		System.out.println("flies at high altitiude");
		
	}
    
}

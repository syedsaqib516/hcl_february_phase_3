package com.gl.abstraction;

interface Calculator
{
	public static final int x=10;// or just intx=10
    public abstract void add();// or just void add();
    void mul();
    
}

interface NewFeatures{
	void newFeature();
}

class NewFeatureImpl 
{
	public void newFeature()
	{
	  System.out.println("implemented");	
	}
}

class SomeClass extends NewFeatureImpl implements NewFeatures 
{
	
}


interface MultipeInherit extends Calculator,NewFeatures
{
	
}

class SimpleCalculator implements  MultipeInherit
{

	@Override
	public void add() {
		System.out.println("simple add");
		
	}

	@Override
	public void mul() {
		System.out.println("simple mul");
		
	}

	@Override
	public void newFeature() {
		System.out.println("new Features");
		
	}
	
}
class ScientificCalculator extends SimpleCalculator implements Calculator,NewFeatures
{

	@Override
	public void add() {
		System.out.println("Scientific add");
		
	}

	@Override
	public void mul() {
		System.out.println("Scientific mul");
		
	}
	
}

public class CalculatorMain
{
	public static void main(String[] args)
	{
		Calculator c1 = new SimpleCalculator();
		c1.add();
		Calculator c2 = new ScientificCalculator();
		c2.add();
		
		new SomeClass().newFeature();
		
	}
}
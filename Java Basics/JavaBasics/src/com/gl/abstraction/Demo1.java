package com.gl.abstraction;

abstract class AbstractClass
{
	public void m1()
	{
		// empty implementation methos
	}
	
	public void m2()
	{
		System.out.println("hello");// concrete method
	}
	
	abstract public void m3();
	
}


class ConcreteClass// concrete class
{	
	public void m1()
	{
		// empty implementation methos
	}
	
	public void m2()
	{
		System.out.println("hello");// concrete method
	}
	
}

public class Demo1 {

}

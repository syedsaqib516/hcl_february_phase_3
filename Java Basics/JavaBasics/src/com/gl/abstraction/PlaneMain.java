package com.gl.abstraction;

public class PlaneMain
{
	public static void main(String[] args) 
	{
		Plane p;//= new Plane();// tight coupling
		
		Plane p1 = new CargoPlane();// upcasting / loose coupling 
		p1.carry();// using parent ref we can access overriden and inherited methods from parent class but not specialized methods
		p1.fly();
		((CargoPlane)(p1)).secureGoods();// downcasting to access specialized methods of child class
		                                 // downcasting can only be done for upcasted objects
		CargoPlane cp=(CargoPlane)p1;
		
	}

}


package com.gl.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcDemo1 
{
	public static void main(String[] args) throws SQLException, ClassNotFoundException
	{
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con=   DriverManager.getConnection("jdbc:mysql://localhost:3306/hcl_24","root","SyedSaqib@123");
		Statement st=  con.createStatement();
//		     st.addBatch("query1");
//		     st.addBatch("query2");
//		     st.addBatch("query3");
//		     st.addBatch("query4");
//		     st.executeBatch();
		     

	st.executeUpdate("INSERT INTO Employee values(106,'Diana','delhi')"); //non select
		st.execute("DELETE FROM Employee WHERE emp_id=102");// select and non select;
		ResultSet rs=  st.executeQuery("SELECT * FROM Employee;");//select
		
		   PreparedStatement   pst=    con.prepareStatement("INSERT INTO Employee values(?,?,?)");
		           pst.setInt(1,102);
		           pst.setString(2, "Vipul");
		           pst.setString(3, "Telangana");
		         int i=  pst.executeUpdate();
		         System.out.println("updated "+i +" record");
		           
		      
		
	  CallableStatement cst=  con.prepareCall("{call myProc(?,?,?)}");
		  cst.setInt(1, 109);
		  cst.setString(2, "Raju");
		  cst.setString(3, "Dholakpur");
		  		  
		 System.out.println(" records updated "+cst.executeUpdate());
		  
		
		System.err.println("employee_id"+"| "+"employee_name" +" | " + "employee_addr");
		System.err.println("-------------------------------------------");
		while(rs.next())
		{
			System.out.println(rs.getInt(1)+"     |      "+rs.getString(2)+"      |      "+rs.getString(3));
		}
		
		con.close();			           	
	}

}

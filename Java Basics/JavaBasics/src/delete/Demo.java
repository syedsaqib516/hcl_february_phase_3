package delete;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

//class MyComparator implements Comparator<Integer>
//{
//
//	@Override
//	public int compare(Integer i1, Integer i2) {
//		
//		return -i1.compareTo(i2);
//	}
//
//	
//	
//}

class MyComparator implements Comparator<Book>
{

	@Override
	public int compare(Book i1, Book i2) {
		
		return -i1.compareTo(i2);
	}

	
	
}

class Book implements Comparable<Book>
{
	int id;
	String name;
	public Book(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	@Override
	public int compareTo(Book bk) {
		if(this.name.compareTo(bk.name)>0) 
		{
			return 1;
		}
		else if(this.name.compareTo(bk.name)<0) {
		return -1;
		}
		else
		{
			return 0;
		}
		
		
	}
	@Override
	public String toString() {
		return "Book [id=" + id + ", name=" + name + "]";
	}

	
	
	
}
public class Demo 
{
	public static void main(String[] args) 
	{
//		ArrayList<Integer> al = new ArrayList<Integer>();// to sort collection must be homogenous and elements must be Comparable
//		al.add(10);
//		al.add(30);
//		al.add(20);
//		al.add(50);
//		al.add(40);
//		Collections.sort(al,new MyComparator());
//		System.out.println(al);
		// compareTo = +ve 0 -ve 
		
//		TreeSet<Book> al = new TreeSet<Book>(new MyComparator());// to sort collection must be homogenous and elements must be Comparable
//		al.add(new Book(1,"abc"));
//		al.add(new Book(3,"hij"));
//		al.add(new Book(4,"mno"));
//		al.add(new Book(5,"klm"));
//		al.add(new Book(2,"def"));
//		System.out.println(al);
		
		ArrayList<Book> al = new ArrayList<Book>();// to sort collection must be homogenous and elements must be Comparable
		al.add(new Book(1,"abc"));
		al.add(new Book(3,"hij"));
		al.add(new Book(4,"mno"));
		al.add(new Book(5,"klm"));
		al.add(new Book(2,"def"));
		Collections.sort(al,new MyComparator());
		System.out.println(al);
		
	}

}

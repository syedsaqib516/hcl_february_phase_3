package ApplicationOfMultiThreading;

public class TicketBookingMain {

	public static void main(String[] args) {
		TicketBooking tb = new TicketBooking();
		Thread t1 = new Thread(tb);
		Thread t2 = new Thread(tb);
		Thread t3 = new Thread(tb);
		t1.setName("user 1");
		t2.setName("user 2");
		t3.setName("user 3");
	    t1.start();
	    t2.start();
	    t3.start();
	}
	
	
}

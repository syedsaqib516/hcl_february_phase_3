package ApplicationOfMultiThreading;

public class TicketBooking implements Runnable
{
	int seatsLeft=2;
	synchronized public void bookTicket() throws InterruptedException
	{
		if(seatsLeft>0) {
		System.out.println("started selecting seat "+Thread.currentThread().getName());
		Thread.sleep(2000);
		System.out.println("making payment for seat "+Thread.currentThread().getName());
		Thread.sleep(2000);
		System.out.println("booking confirmed "+Thread.currentThread().getName());
		System.out.println("seat no "+seatsLeft+" is booked by "+Thread.currentThread().getName());
		  seatsLeft -=1;
		}
		else
		{
			System.out.println("booking full , no seats left for "+Thread.currentThread().getName());
		}
	}

	@Override
	public void run() {
		try {
			bookTicket();
		}
		catch(Exception e)
		{
		  System.out.println(e.getMessage());
		}
	}

}

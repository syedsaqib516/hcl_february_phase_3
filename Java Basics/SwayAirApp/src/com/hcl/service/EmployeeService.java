package com.hcl.service;

import java.sql.SQLException;

import com.hcl.beans.Employee;

public interface EmployeeService {
	Employee addEmployee(Employee emp) throws SQLException;
	public Employee updateEmployee();
	public Employee getEmployee(int empId);
	public void deleteEmployee();
	
}

package com.hcl.service;

import java.sql.SQLException;

import com.hcl.beans.Employee;
import com.hcl.dao.EmployeeDao;
import com.hcl.dao.EmployeeDaoImpl;

public class EmployeeServiceImpl implements EmployeeService
{
	EmployeeDao dao = new EmployeeDaoImpl();
	
	@Override
	public Employee addEmployee(Employee emp) throws SQLException {

		return dao.addEmployee(emp);
	}

	@Override
	public Employee updateEmployee() {

		return dao.updateEmployee();
	}

	@Override
	public Employee getEmployee(int empId) {

		return dao.getEmployee(empId);
	}

	@Override
	public void deleteEmployee() {
      dao.deleteEmployee();

	}

}

package com.hcl.dao;

import java.sql.SQLException;

import com.hcl.beans.Employee;

public interface EmployeeDao 
{
	public Employee addEmployee(Employee emp) throws SQLException;
	public Employee updateEmployee();
	public Employee getEmployee(int empId);
	public void deleteEmployee();

}

package com.hcl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import com.hcl.beans.Employee;
import com.hcl.util.DbConnection;

public class EmployeeDaoImpl implements EmployeeDao {

	Connection con;

	public EmployeeDaoImpl()
	{
		con=	DbConnection.getConn();
	}


	@Override
	public Employee addEmployee(Employee emp) throws SQLException {
		Statement st=  con.createStatement();
		int empId= emp.getEmpId();
		String empName=emp.getEmpName();
		String empAddr=emp.getEmpAddr();

		PreparedStatement   pst=    con.prepareStatement("INSERT INTO Employee values(?,?,?)");
		pst.setInt(1,empId);
		pst.setString(2, empName);
		pst.setString(3, empAddr);
		int i=  pst.executeUpdate();
		System.out.println("updated "+i +" record");

		if(i>0)
			return  emp ;
		else
			return null;
	}

	@Override
	public Employee updateEmployee() {

		return null;
	}

	@Override
	public Employee getEmployee(int empId) {

		return null;
	}

	@Override
	public void deleteEmployee() {


	}

}

create table User(userName varchar(20),userId int,email varchar(20) primary key,password varchar(20));
create table Item(id int primary key,itemName varchar(20), availableQuantity int,price double);
create table AddItem(id int,name varchar(20),quantity int,price double,date Date,email varchar(20),foreign key(id) references Item(id));
create table Bill(email varchar(20) primary key,noOfItems int,totalBill double,date Date);

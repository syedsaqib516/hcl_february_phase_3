package com.hcl.loggers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.time.LocalDateTime;

//This is for storing the logs about all operations........
public class CustomLogger {

	static String filepath = "Log2.txt";
	static FileWriter fw;

	public static void storeLog(String message) {
		try {
			if (fw == null) {
				fw = new FileWriter(filepath, true);
			}

			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			LocalDateTime dt = LocalDateTime.now();
			pw.println(message + "---" + dt);
			pw.flush();
			pw.close();
			bw.close();
			fw.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package com.hcl.admindao;

import com.hcl.dto.Admin;
import com.hcl.exceptions.InvalidLoginCredentialsException;
import com.hcl.loggers.CustomLogger;
import com.hcl.loggers.LoggerImpl;

//In this i implimented login for Admin with final variable(fixed credentials)....
public class AdminDaoImpl implements AdminDao {
	Admin a1 = new Admin();
	LoggerImpl log = new LoggerImpl();
	private Admin a;

	@Override
	public Admin adminLogin(String email, String pwd) {// this method for admin login..by implementing the Admin Dao...
		if ((a1.getEmail().equals(email)) && (a1.getPassword().equals(pwd))) {
			a = a1;
			CustomLogger.storeLog("The user " + a.getEmail() + " login successfully......");// logger
			log.info("\"The user " + a.getEmail() + " login successfully......\"");

		} else {
			CustomLogger.storeLog("The user " + a.getEmail() + " is invalid ......");// logger
			log.info("\"The user " + a.getEmail() + " is Invalid.....\"");
			throw new InvalidLoginCredentialsException("You are a invalid Admin.....");// custom exception
		}
		return a;
	}

}

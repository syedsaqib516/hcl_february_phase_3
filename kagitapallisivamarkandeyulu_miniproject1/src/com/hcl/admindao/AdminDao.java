package com.hcl.admindao;

import com.hcl.dto.Admin;

//this is admin dao....
public interface AdminDao {
	public Admin adminLogin(String email, String pwd);

}

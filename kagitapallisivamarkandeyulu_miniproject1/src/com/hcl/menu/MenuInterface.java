package com.hcl.menu;

import java.util.Scanner;

import com.hcl.admindao.AdminDao;
import com.hcl.admindao.FactoryClass;
import com.hcl.dto.Admin;
import com.hcl.dto.User;
import com.hcl.threads.AdminThread;
import com.hcl.threads.UserThread;
import com.hcl.userdao.UserFactory;

public class MenuInterface {

	static Scanner sc = new Scanner(System.in);// it is used for Scanning the inputs from both user and admin
	static AdminDao ad = FactoryClass.getAdminDao();// This is for admin login...

	public static void main(String[] args) {// This is main method execution starts from here....And also here user
											// signup ,user& admin signin....

		int ch;
		System.out.println("Welcome to HCL Cafeteria ...");// welcome page for both users and admin...

		do {
			System.out.println("Please choose your option....");
			System.out.println("1- for User SignUp ......");
			System.out.println("2- Admin login.... plz enter email,pwd.........");
			System.out.println("3- User login.....");
			System.out.println("4-Exit");
			ch = sc.nextInt();

			switch (ch) {
			case 1:// User signup......
				System.out.println("--User register- enter' name','user id', 'email','pwd'");
				System.out.println("Enter name...");
				String name = sc.next();
				System.out.println("Enter id..");
				int id = sc.nextInt();
				System.out.println("Enter email ...");
				String email = sc.next();
				System.out.println("Enter password must be greater than 4 characters...");
				String pwd = sc.next();

				try {
					UserFactory.getUsersignup(name, id, email, pwd);
					System.out.println("--detail:" + new User<>(name, id, email, pwd));
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println(e.getMessage());
				}
				break;
			case 2:// Admin login,No need to signup because only one authorized admin there name is
					// _Siva_....
					// for checking purpose i disclose the Admin credentials below please use those
					// credentials for admin login......

				System.out.println(
						"--Admin login-  'email','pwd'..only .for checking purpose i disclose the Admin credentials below please use those credentials for admin login");
				System.out.println("email:  siva@123");
				System.out.println("password:   siva123");
				String adminEmail = sc.next();// siva@123
				String adminPwd = sc.next();// siva123
				Admin a = null;
				try {
					a = ad.adminLogin(adminEmail, adminPwd);
				} catch (Exception e) {
					// TODO: handle exception
					e.getMessage();
				}
				if (a != null) {

					System.out.println("--detail:" + a);
					AdminThread at = new AdminThread(a);
					// After admin loginsuccessfull. admin authorized to access the admin
					at.start();
					try {

						at.join();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						System.out.println(e.getMessage());
					}
				} else {
					System.out.println("You are not a valid Admin");
				}
				break;

			case 3:// user login....
				System.out.println("-----TO Login--- 'user email','pwd'-"); //
				System.out.println("Enter email ...");
				String userEmail = sc.next();
				System.out.println("Enter password...");
				String pwd1 = sc.next();
				User u = null;
				try {
					u = UserFactory.getUserLogin(userEmail, pwd1);
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println(e.getMessage());
				}
				if (u != null) {

					UserThread ut = new UserThread(u);
					ut.start();
					try {

						ut.join();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						System.out.println(e.getMessage());
					} /// After user login successfull user is authorized to
						/// access the user page....

				} else {
					System.out.println("You are not a valid user");
				}
				break;
			case 4:

				System.out.println("Thank you visit again...");
				ch = 0;
				break;
			default:
				break;
			}

		} while (ch != 0);
	}

}

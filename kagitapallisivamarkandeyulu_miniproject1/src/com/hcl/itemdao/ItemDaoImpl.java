package com.hcl.itemdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hcl.connection.SqlConnection;
import com.hcl.dto.AddItem;
import com.hcl.dto.Bill;
import com.hcl.dto.Item;
import com.hcl.exceptions.ItemIsAlreadyExistedException;
import com.hcl.exceptions.NoItemException;
import com.hcl.loggers.CustomLogger;
import com.hcl.loggers.LoggerImpl;

public class ItemDaoImpl implements ItemDao {
	private Connection con;
	private Statement sm;
	private PreparedStatement ps;
	private LoggerImpl log = new LoggerImpl();
	private Date d;

	public ItemDaoImpl() {
		con = SqlConnection.getConnection();
		// TODO Auto-generated constructor stub
	}

	@Override
	public Item addItem(Item item) {
		int pos = 0;
		Item im = null;
		List<Item> it = new ArrayList<>();

		String query2 = "insert into Item values(?,?,?,?)";
		try {
			it = getItems();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}
		for (int i = 0; i < it.size(); i++) {
			if (it.get(i).getId() == item.getId()) {
				pos++;
				break;
			}
		}
		if (pos == 0) {
			try {
				ps = con.prepareStatement(query2);
				ps.setInt(1, item.getId());
				ps.setString(2, item.getItemName());
				ps.setInt(3, item.getAvailableQuantity());
				ps.setDouble(4, item.getPrice());
				ps.executeUpdate();
				im = new Item(item.getId(), item.getItemName(), item.getAvailableQuantity(), item.getPrice());
				log.info("Item successfully Added...");
				CustomLogger.storeLog("Item successfully Added...");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}

		} else {
			log.warn("Item already existed with this id...");
			CustomLogger.storeLog("Item already existed with this id...");
			throw new ItemIsAlreadyExistedException("Item already existed with this id...");
		}
		return im;
	}

	@Override
	public void deleteItem(int id) {
		// TODO Auto-generated method stub
		int pos = 0;
		List<Item> it = new ArrayList<>();
		String query2 = "delete from Item where id=" + id + "";
		try {
			it = getItems();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}
		for (int i = 0; i < it.size(); i++) {
			if (it.get(i).getId() == id) {
				pos++;
				break;
			}
		}
		if (pos == 1) {
			try {
				ps = con.prepareStatement(query2);
				ps.executeUpdate();
				log.info("Item successfully deleted...");
				CustomLogger.storeLog("Item successfully deleted..");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}

		} else {
			log.warn("Item is not existed with this id...");
			CustomLogger.storeLog("Item is not existed with this id...");
			throw new NoItemException("Item is not existed with this id...");
		}

	}

	@Override
	public Item updateAvailbleQuantity(int id, int quantity) {
		// TODO Auto-generated method stub
		int pos = 0;
		Item im = null;
		List<Item> it = new ArrayList<>();
		String query2 = "update  Item set availableQuantity=" + quantity + " where id=" + id + "";
		try {
			it = getItems();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}
		for (int i = 0; i < it.size(); i++) {
			if (it.get(i).getId() == id) {
				im = it.get(i);
				pos++;
				break;
			}
		}
		if (pos == 1) {
			try {
				ps = con.prepareStatement(query2);
				ps.executeUpdate();
				log.info("Item successfully updated...");
				CustomLogger.storeLog("Item successfully updated..");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}

		} else {
			log.warn("Item is not existed with this id...");
			CustomLogger.storeLog("Item is not existed with this id...");
			throw new NoItemException("Item is not existed with this id...");
		}

		return im;
	}

	@Override
	public Item updateItem(int id, double price) {
		// TODO Auto-generated method stub
		int pos = 0;
		Item im = null;
		List<Item> it = new ArrayList<>();
		String query2 = "update  Item set price=" + price + " where id=" + id + "";
		try {
			it = getItems();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}
		for (int i = 0; i < it.size(); i++) {
			if (it.get(i).getId() == id) {
				im = it.get(i);
				pos++;
				break;
			}
		}
		if (pos == 1) {
			try {
				ps = con.prepareStatement(query2);
				ps.executeUpdate();
				im.setPrice(price);
				log.info("Item successfully updated...");
				CustomLogger.storeLog("Item successfully updated..");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}

		} else {
			log.warn("Item is not existed with this id...");
			CustomLogger.storeLog("Item is not existed with this id...");
			throw new NoItemException("Item is not existed with this id...");
		}

		return im;
	}

	@Override
	public List<Item> getItems() {
		// TODO Auto-generated method stub
		Item im = null;
		List<Item> it = new ArrayList<>();
		String query = "select * from Item";
		try {
			sm = con.createStatement();
			ResultSet rs = sm.executeQuery(query);
			while (rs.next()) {
				im = new Item(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4));
				it.add(im);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		if (!it.isEmpty()) {
			CustomLogger.storeLog("successfully get the items..");
		} else {
			throw new NoItemException("No items Available");
		}
		return it;

	}

	@SuppressWarnings("deprecation")
	@Override
	public void orderItem(int id, String email, int quantity) {
		// TODO Auto-generated method stub
		int pos = 0;
		Item im = null;
		List<Item> it = new ArrayList<>();
		String query2 = "insert into AddItem values(?,?,?,?,?,?)";
		try {
			it = getItems();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}
		for (int i = 0; i < it.size(); i++) {
			if (it.get(i).getId() == id) {
				pos++;
				im = new Item(it.get(i).getId(), it.get(i).getItemName(), it.get(i).getAvailableQuantity(),
						it.get(i).getPrice());
				break;
			}
		}

		if (pos == 1 && im.getAvailableQuantity() > 0) {
			try {
				ps = con.prepareStatement(query2);
				if (im.getAvailableQuantity() - quantity < 0 || quantity < 0) {
					throw new NoItemException(
							"present only " + im.getAvailableQuantity() + " plates available or invalid quantity..");
				}
				ps.setInt(1, im.getId());
				ps.setString(2, im.getItemName());
				ps.setInt(3, quantity);
				ps.setDouble(4, (im.getPrice() * quantity));
				d = new Date();
				ps.setDate(5, new java.sql.Date(d.getYear(), d.getMonth(), d.getDay()));
				ps.setString(6, email);
				ps.executeUpdate();

				updateQuantity(id, im.getAvailableQuantity(), quantity);
				updateBill(email, quantity, (im.getPrice() * quantity));
				log.info("Item successfully Added...");
				CustomLogger.storeLog("Item successfully Added...");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}

		} else {
			log.warn("Item is not available...");
			CustomLogger.storeLog("Item is not available.....");
			throw new ItemIsAlreadyExistedException("Item is not available.....");
		}

	}

	@Override
	public Bill getBill(String email) {
		// TODO Auto-generated method stub
		Bill im = null;
		String query = "select * from Bill where email='" + email + "'";
		try {
			sm = con.createStatement();
			ResultSet rs = sm.executeQuery(query);
			while (rs.next()) {
				im = new Bill(rs.getString(1), rs.getInt(2), rs.getDouble(3), rs.getDate(4));
				break;

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		if (im != null) {
			CustomLogger.storeLog("successfully get the bill..");
		} else {
			throw new NoItemException("No bills available...");
		}
		return im;
	}

	@Override
	public List<Bill> getBills() {
		// TODO Auto-generated method stub
		Bill im = null;
		List<Bill> b = new ArrayList<>();
		String query = "select * from Bill";
		try {
			sm = con.createStatement();
			ResultSet rs = sm.executeQuery(query);
			while (rs.next()) {
				im = new Bill(rs.getString(1), rs.getInt(2), rs.getDouble(3), rs.getDate(4));
				b.add(im);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		if (!b.isEmpty()) {

			CustomLogger.storeLog("successfully get the bills..");
		} else {
			throw new NoItemException("No bills available...");
		}
		return b;
	}

	public void updateQuantity(int id, int available, int quantity) {
		String query = "update Item set availableQuantity=" + (available - quantity) + " where id=" + id + "";
		try {
			sm = con.createStatement();
			sm.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
		}

	}

	@SuppressWarnings("deprecation")
	public void updateBill(String email, int quantity, double price) {
		AddItem im = null;
		List<AddItem> it = new ArrayList<>();
		String query2 = "insert into Bill values(?,?,?,?)";
		String query3 = "update Bill set totalBill=?,noOfItems=?,date=? where email='" + email + "'";
		try {
			it = getOrderItems(email);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
		}
		int items = 0;
		double amount = 0;
		Bill b = getBill(email);
		if (!it.isEmpty()) {

			for (int i = 0; i < it.size(); i++) {
				items += it.get(i).getQuantity();
				amount += it.get(i).getPrice();
			}
		}
		if (b != null) {
			try {
				ps = con.prepareStatement(query3);
				ps.setDouble(1, amount);
				ps.setInt(2, items);
				d = new Date();
				ps.setDate(3, new java.sql.Date(d.getYear(), d.getMonth(), d.getDay()));
				ps.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				ps = con.prepareStatement(query2);
				ps.setString(1, email);
				ps.setDouble(2, price);
				ps.setInt(3, quantity);
				d = new Date();
				ps.setDate(4, new java.sql.Date(d.getYear(), d.getMonth(), d.getDay()));
				ps.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@Override
	public List<AddItem> getOrderItems(String email) {
		AddItem im = null;
		List<AddItem> it = new ArrayList<>();
		String query = "select * from AddItem where email='" + email + "'";
		try {
			sm = con.createStatement();
			ResultSet rs = sm.executeQuery(query);
			while (rs.next()) {
				im = new AddItem(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4), rs.getDate(5),
						rs.getString(6));
				it.add(im);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		if (!it.isEmpty()) {

			CustomLogger.storeLog("successfully get the order items..");
		} else {
			throw new NoItemException("No order items available with this email...");
		}
		return it;
	}

	@Override
	public List<AddItem> getAllOrders() {
		AddItem im = null;
		List<AddItem> it = new ArrayList<>();
		String query = "select * from AddItem ";
		try {
			sm = con.createStatement();
			ResultSet rs = sm.executeQuery(query);
			while (rs.next()) {
				im = new AddItem(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4), rs.getDate(5),
						rs.getString(6));
				it.add(im);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		if (!it.isEmpty()) {

			CustomLogger.storeLog("successfully get the order items..");
		} else {
			throw new NoItemException("No order items available with this email...");
		}
		return it;

	}

	@Override
	public List<Bill> getBillsByDate() {
		// TODO Auto-generated method stub
		Bill im = null;
		List<Bill> b = new ArrayList<>();
		LocalDate dd = LocalDate.now();

		String query = "select * from Bill where date like '" + dd + "'";
		try {
			ps = con.prepareStatement(query);
			d = new Date();

			ResultSet rs = ps.executeQuery(query);
			while (rs.next()) {
				im = new Bill(rs.getString(1), rs.getInt(2), rs.getDouble(3), rs.getDate(4));
				b.add(im);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		if (!b.isEmpty()) {

			CustomLogger.storeLog("successfully get the bills..");
		} else {
			throw new NoItemException("No bills available...");
		}
		return b;
	}

	@Override
	public List<Bill> getBillsByMonth() {
		// TODO Auto-generated method stub
		Bill im = null;
		List<Bill> b = new ArrayList<>();
		LocalDate dd = LocalDate.now();
		System.out.println(dd.getYear());
		String s = "%" + dd.getMonthValue() + "-__";

		String query = "select * from Bill where date like '" + s + "'";
		try {
			ps = con.prepareStatement(query);
			d = new Date();

			ResultSet rs = ps.executeQuery(query);
			while (rs.next()) {
				im = new Bill(rs.getString(1), rs.getInt(2), rs.getDouble(3), rs.getDate(4));
				b.add(im);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		if (!b.isEmpty()) {

			CustomLogger.storeLog("successfully get the bills..");
		} else {
			throw new NoItemException("No bills available...");
		}
		return b;
	}
}
package com.hcl.itemdao;


import java.util.List;

import com.hcl.dto.AddItem;
import com.hcl.dto.Bill;
import com.hcl.dto.Item;

public interface ItemDao {
	public void deleteItem(int id);

	public Item updateItem(int id, double price);

	public List<Item> getItems();

	public Bill getBill(String email);

	void orderItem(int id, String email, int quantity);

	Item addItem(Item item);

	List<Bill> getBills();
	List<Bill> getBillsByDate();

	public Item updateAvailbleQuantity(int id, int quantity);

	List<AddItem> getOrderItems(String email);

	List<AddItem> getAllOrders();

	List<Bill> getBillsByMonth();
}

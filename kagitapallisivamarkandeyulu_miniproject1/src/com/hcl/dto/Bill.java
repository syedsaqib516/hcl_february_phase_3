package com.hcl.dto;

import java.sql.Date;

public class Bill {//Total bill...
	private String email;
	private int noOfItems;
	private double totalBill;
	private Date date;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getNoOfItems() {
		return noOfItems;
	}

	public void setNoOfItems(int noOfItems) {
		this.noOfItems = noOfItems;
	}

	public double getTotalBill() {
		return totalBill;
	}

	public void setTotalBill(double totalBill) {
		this.totalBill = totalBill;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Bill(String email, int noOfItems, double totalBill, Date date) {
		super();
		this.email = email;
		this.noOfItems = noOfItems;
		this.totalBill = totalBill;
		this.date = date;
	}

	public Bill() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Bill [email=" + email + ", noOfItems=" + noOfItems + ", totalBill=" + totalBill + ", date=" + date
				+ "]";
	}

}

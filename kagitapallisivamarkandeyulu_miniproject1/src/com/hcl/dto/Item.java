package com.hcl.dto;

public class Item {//food items.....
	private int id;
	private String itemName;
	private int availableQuantity;
	private double price;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(int availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Item() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Item(int id, String itemName, int availableQuantity, double price) {
		super();
		this.id = id;
		this.itemName = itemName;
		this.availableQuantity = availableQuantity;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", itemName=" + itemName + ", availableQuantity=" + availableQuantity + ", price="
				+ price + "]";
	}

}

package com.hcl.dto;

import java.util.Date;

public class AddItem {
	private int id;
	private String name;
	private int quantity;
	private double price;
	private Date date;
	private String email;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public AddItem() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AddItem(int id, String name, int quantity, double price, Date date, String email) {
		super();
		this.id = id;
		this.name = name;
		this.quantity = quantity;
		this.price = price;
		this.date = date;
		this.email = email;
	}

	@Override
	public String toString() {
		return "AddItem [id=" + id + ", name=" + name + ", quantity=" + quantity + ", price=" + price + ", date=" + date
				+ ", email=" + email + "]";
	}

}

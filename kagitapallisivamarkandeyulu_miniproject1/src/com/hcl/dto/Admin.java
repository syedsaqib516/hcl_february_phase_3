package com.hcl.dto;

// this is for admin ...
public final class Admin {

	private final String EMAIL = "siva@123";// login credential for admin.....email & password..because admin is
											// authorized person...
	private final String PASSWORD = "siva123";// i give the access for only one person.....

	public String getEmail() {
		return EMAIL;
	}

	public Admin() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Admin [email=" + EMAIL + ", password=" + PASSWORD + "]";
	}

	public String getPassword() {
		return PASSWORD;
	}

}

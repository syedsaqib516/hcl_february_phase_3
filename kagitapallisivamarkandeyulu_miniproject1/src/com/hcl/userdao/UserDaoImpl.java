package com.hcl.userdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hcl.connection.SqlConnection;
import com.hcl.dto.User;
import com.hcl.exceptions.InvalidLoginCredentialsException;
import com.hcl.exceptions.InvalidSignupCredentialsException;
import com.hcl.loggers.CustomLogger;
import com.hcl.loggers.LoggerImpl;

public class UserDaoImpl implements UserDao {
	private static Connection con = SqlConnection.getConnection();
	private static PreparedStatement ps;
	private static LoggerImpl log = new LoggerImpl();

	@Override
	public User userSignUp(User us) {
		List<User> ul = new ArrayList<>();
		User u;
		int pos = 0;
		String query = "select * from User";
		String query2 = "insert into User values(?,?,?,?)";

		try {
			ps = con.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				u = new User(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getString(4));
				ul.add(u);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
		for (int i = 0; i < ul.size(); i++) {
			if (ul.get(i).getUserId() == (us.getUserId()) && ul.get(i).getPassword().equals(us.getPassword())) {
				pos++;
			}
		}
		if (pos == 0) {
			if (us.getEmail().contains("@hcl.com") && us.getPassword().length() > 4) {
				try {
					ps = con.prepareStatement(query2);
					ps.setString(1, us.getUserName());
					ps.setInt(2, us.getUserId());
					ps.setString(3, us.getEmail());
					ps.setString(4, us.getPassword());
					ps.executeUpdate();
					log.info("User signup successfully.....");
					CustomLogger.storeLog("User signup successfully....");

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.getMessage();
				}
			} else {
				log.warn("Invalid credentials.....");
				CustomLogger.storeLog("The email consists --@hcl.com--,password length more than 4 characters.....");
				throw new InvalidSignupCredentialsException(
						"The email consists --@hcl.com--,password length more than 4 characters...");
			}

		}
		return null;

	}

	@Override
	public User userLogin(String email, String password) {
		List<User> ul = new ArrayList<>();
		User u = null;
		int pos = 0;
		String query = "select * from User";

		try {
			ps = con.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				u = new User(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getString(4));
				ul.add(u);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
		for (int i = 0; i < ul.size(); i++) {
			if (ul.get(i).getEmail().equals(email) && ul.get(i).getPassword().equals(password)) {
				u = ul.get(i);
				pos++;
				break;
			}
		}
		if (pos == 1) {
			log.info("User login successfully.....");
			CustomLogger.storeLog("User signup successfully....");

		} else {
			log.warn("Invalid User.....");
			CustomLogger.storeLog("Invalid user.....");
			throw new InvalidLoginCredentialsException("Invalid user...");
		}
		return u;

		// TODO Auto-generated method stub
	}// this class for developing the signup and login for user by implementing

}

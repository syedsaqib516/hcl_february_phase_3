package com.hcl.userdao;

import com.hcl.dto.User;

public interface UserDao {// This interface for initialize the signup and login for user...
	public User userSignUp(User us);

	public User userLogin(String email, String password);
}

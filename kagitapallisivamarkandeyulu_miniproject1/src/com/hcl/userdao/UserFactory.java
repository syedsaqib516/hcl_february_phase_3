package com.hcl.userdao;

import com.hcl.dto.User;

public class UserFactory {
	private static UserDao dao=new UserDaoImpl();
	
public static User getUserLogin(String email,String pwd) {
	User u=dao.userLogin(email,pwd);
	return u;
		
}
public static void getUsersignup(String name,int id,String email,String pwd) {
	dao.userSignUp(new User<Object>(name, id, email, pwd));
}

}

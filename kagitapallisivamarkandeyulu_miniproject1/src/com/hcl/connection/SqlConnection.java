package com.hcl.connection;

import java.sql.Connection;
import java.sql.DriverManager;

public class SqlConnection {
	private static Connection con = null;
	private static String url = "jdbc:mysql://localhost:3306/miniproject";
	private static String username = "root";
	private static String password = "siva123";
	private static String driver = "com.mysql.cj.jdbc.Driver";

	private SqlConnection() {

	}

	public static synchronized Connection getConnection() {
		try {
			if (con == null) {
				Class.forName(driver);
				con = DriverManager.getConnection(url, username, password);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}
}

package com.hcl.threads;

import java.util.List;
import java.util.Scanner;

import com.hcl.dto.AddItem;
import com.hcl.dto.Bill;
import com.hcl.dto.Item;
import com.hcl.dto.User;
import com.hcl.itemdao.ItemDao;
import com.hcl.itemdao.ItemDaoImpl;



public class UserThread extends Thread {// It is for user thread....

	Scanner sc = new Scanner(System.in);// it is used for Scanning the inputs from both user and admin

	private ItemDao dao=new ItemDaoImpl();
	User u;

	public UserThread() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserThread(User u) {
		super();
		this.u = u;

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

		menuForUser(u.getUserId(), u.getUserName());

	}

	public void menuForUser(int userId, String name) {// This is for user page...

		int choice;
		System.out.println("Welcome to User page...");

		do {
			System.out.println("Please choose your option....");
			System.out.println("1-add order items...");
			System.out.println("2-get order items");
			System.out.println("3-get all items");
			System.out.println("4-get Bills");
			System.out.println("5-Logout.....");

			choice = sc.nextInt();

			switch (choice) {
		   case 1:
			   try {
			   List<Item> il=dao.getItems();
			   il.forEach(a->System.out.println(a));
			   }catch (Exception e) {
				// TODO: handle exception
				   System.err.println(e.getMessage());
			}
			   System.out.println("Enter the number of items");
			int c=sc.nextInt();
			while(c>0) {
				  System.out.println("Enter the values...");
				  System.out.println("Enter the item id...");
				  int id=sc.nextInt();
				  System.out.println("Enter the no of plates...");
				  int noOfPlates=sc.nextInt();
			try {
				dao.orderItem(id, name, noOfPlates );
			}catch (Exception e) {
					// TODO: handle exception
					System.err.println(e.getMessage());
				}
			c--;
			}
		
			
				break;
			case 2:System.out.println("Your order items...");
			
			try {
				List<AddItem> al=dao.getOrderItems(name);
				al.forEach(b->System.out.println(b));
			}catch (Exception e) {
				// TODO: handle exception
				System.err.println(e.getMessage());
			}

				break;
			case 3:System.out.println("Get all items...");
				List<Item> il=dao.getItems();
				 il.forEach(m->System.out.println(m));
				
				break;
				
			case 4:

				System.out.println("Your bill...");
				try {
					Bill lb=dao.getBill(name);
					System.out.println(lb);
				}catch (Exception e) {
					// TODO: handle exception
					System.err.println(e.getMessage());
				}

					break;
			case 5:
				System.out.println("Thank you visit again...");
				choice = 0;
				break;

			default:
				break;

			}
		} while (choice != 0);

	}
}

package com.hcl.threads;

import java.util.List;
import java.util.Scanner;

import com.hcl.dto.AddItem;
import com.hcl.dto.Admin;
import com.hcl.dto.Bill;
import com.hcl.dto.Item;
import com.hcl.itemdao.ItemDao;
import com.hcl.itemdao.ItemDaoImpl;




public class AdminThread extends Thread {// It is for admin thread....

	Scanner sc = new Scanner(System.in);// it is used for Scanning the inputs from both user and admin

	Admin a;
    private ItemDao dao=new ItemDaoImpl();
	public AdminThread() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AdminThread(Admin a) {
		super();
		this.a = a;
	}

	@Override
	public void run() {
		try {
			menuForAdmin(a.getEmail());
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
	}

	public void menuForAdmin(String email) {// This is for Admin page...

		int choice;
		System.out.println("Welcome to Admin page ...");
		do {
			System.out.println(" Please choose your option....");
			System.out.println("1-Add Item");
			System.out.println("2-update price");
			System.out.println("3-delete item");
			System.out.println("4-Get Items");
			System.out.println("5-Get all Bills");
			System.out.println("6-update quantity");
			System.out.println("7-get all orders");
			System.out.println("8-get Today Bills");
			System.out.println("9-get This month Bills");
			System.out.println("10-Logout");
			
			choice = sc.nextInt();
       
			switch (choice) {
			case 1:System.out.println("Enter the Item dtails....");
			System.out.println("Enter the Item Id in number example:'1'...");
			int id=sc.nextInt();
			System.out.println("enter the item name example:'Curd_Rice'...");
			String name=sc.next();
			System.out.println("Enter the Item quantity in number example:'1'...");
			int quantity=sc.nextInt();
			System.out.println("Enter the Item price in number example:'100.00'...");
			Item i=null;
			double price=sc.nextDouble();
			try {
				 i=dao.addItem(new Item(id, name, quantity, price));
				 System.out.println(i);
			}catch (Exception e) {
				// TODO: handle exception
				System.err.println(e.getMessage());
			}
			
			break;	
			case 2:System.out.println("your items....");
				try {
					   List<Item> il=dao.getItems();
					   il.forEach(a->System.out.println(a));
				}catch (Exception e) {
					// TODO: handle exception
					System.err.println(e.getMessage());
				}
			System.out.println("Enter the  dtails....");
			System.out.println("Enter the Item Id in number example:'1'...");
			int itemId=sc.nextInt();
			System.out.println("Enter the new Price...");
			double amount=sc.nextDouble();
			try {
				i=dao.updateItem(itemId,amount);
				System.out.println(i);
			}catch (Exception e) {
				// TODO: handle exception
				System.err.println(e.getMessage());
			}
			break;	
			case 3:
				try {
					   List<Item> il=dao.getItems();
					   il.forEach(a->System.out.println(a));
						}catch (Exception e) {
							// TODO: handle exception
							System.err.println(e.getMessage());
						}
				System.out.println("Enter the  dtails....");
				System.out.println("Enter the Item Id in number example:'1'...");
				int deleteId=sc.nextInt();
				try {
					dao.deleteItem(deleteId);
				}catch (Exception e) {
					// TODO: handle exception
					System.err.println(e.getMessage());
				}
				break;
			case 4:System.out.println("your items....");
				try {
					   List<Item> il=dao.getItems();
					   il.forEach(a->System.out.println(a));
						}catch (Exception e) {
							// TODO: handle exception
							System.err.println(e.getMessage());
						}
				break;
			case 5:System.out.println("your Bills....");
			
			try {
				List<Bill> lb=dao.getBills();
				lb.forEach(a->System.out.println(a));
					}catch (Exception e) {
						// TODO: handle exception
						System.err.println(e.getMessage());
					}
			break;
			case 6:
				try {
					   List<Item> il=dao.getItems();
					   il.forEach(a->System.out.println(a));
			}catch (Exception e) {
				// TODO: handle exception
				System.err.println(e.getMessage());
			}
				System.out.println("Enter the details..");
				System.out.println("Enter the Item Id in number example:'1'...");
				int updateId=sc.nextInt();
				System.out.println("Enter the new Quantity in number example:'1'...");
				int updateQuantity=sc.nextInt();
				try {
					dao.updateAvailbleQuantity(updateId, updateQuantity);
				}catch (Exception e) {
					// TODO: handle exception
					System.err.println(e.getMessage());
				}
				break;
			case 7:
                try {
                	List<AddItem> a=dao.getAllOrders();
                	a.forEach(n->System.out.println(n));
                }catch (Exception e) {
					// TODO: handle exception
                	System.err.println(e.getMessage());
				}
			
				break;
			case 8:System.out.println("Your today bills..");
				try {
					List<Bill> bb=dao.getBillsByDate();
				bb.forEach(n->System.out.println(n));
				}catch (Exception e) {
					// TODO: handle exception
				}
				break;
			case 9:
				System.out.println("This month bills..");
				try {
					List<Bill> bb=dao.getBillsByMonth();
				bb.forEach(n->System.out.println(n));
				}catch (Exception e) {
					// TODO: handle exception
				}
				break;
			case 10:
				System.out.println("Thank you visit again...");
				choice = 0;
				break;
			default:
				break;
			}

		} while (choice != 0);

	}
}

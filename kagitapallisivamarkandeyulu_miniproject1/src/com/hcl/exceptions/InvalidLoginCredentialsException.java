package com.hcl.exceptions;

public class InvalidLoginCredentialsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidLoginCredentialsException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}

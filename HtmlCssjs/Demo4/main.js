console.log("hello world!");

//Primitive Data Types
//String
//Number
//Boolean
//Null
//Undefined
//Symbol
//BigInt
//Array

//non primitive
//Object

var name = "John";
var age = 30;
var isMarried = false;
var job = null;
var car;
var sym = Symbol('foo');
var bigInt = 1234567890123456789012345678901234567890n;
var arr = [1,2,3,4,5];

var person = {'name': 'John', 'age': 30, 'isMarried': false, 'job': null, 'car': undefined, 'sym': Symbol('foo'), 'bigInt': 1234567890123456789012345678901234567890n, 'arr': [1,2,3,4,5]};
console.log(person);

//Type Conversion
var x = 5;
console.log(x);
console.log(typeof x);
x = String(x);
console.log(x);
console.log(typeof x);

var y = "5";
console.log(y);
console.log(typeof y);
y = Number(y);
console.log(y);
console.log(typeof y);

var z = "5";    
console.log(z);
console.log(typeof z);
z = Boolean(z);
console.log(z);
console.log(typeof z);

//Operators
var a = 5;
var b = 10;
console.log(a+b);   
console.log(a-b);
console.log(a*b);
console.log(a/b);
console.log(a%b);
console.log(a**b);  //exponentiation
console.log(a++);
console.log(a--);
console.log(++a);
console.log(--a);
console.log(a+=b);
console.log(a-=b);
console.log(a*=b);
console.log(a/=b);
console.log(a%=b);
console.log(a**=b); 

//Comparison Operators
var c = 5;
var d = "5";
console.log(c==d);
console.log(c===d);
console.log(c!=d);
console.log(c!==d);
console.log(c>d);
console.log(c<d);
console.log(c>=d);
console.log(c<=d);

//Logical Operators
var e = 5;
var f = 10;
console.log(e>f && e<f);
console.log(e>f || e<f);
console.log(!(e>f));

//String Operators
var g = "Hello";
var h = "World";
console.log(g + " " + h);
console.log(g += h);

//Conditional (Ternary) Operator
var i = 5;
var j = 10;
console.log(i>j ? "i is greater than j" : "i is less than j");

//Functions
function add(a, b) {
    return a+b;
}
console.log(add(5, 10));
console.log(add("abc", "xyz"));

//Arrow Functions
var add = (a, b) => a+b;
console.log(add(5, 10));
console.log(add("abc", "xyz"));

//Objects
var person = {
    name: "John",
    age: 30,
    isMarried: false,
    job: null,
    car: undefined,
    sym: Symbol('foo'),
    bigInt: 1234567890123456789012345678901234567890n,
    arr: [1,2,3,4,5]
};
console.log(person.name);
console.log(person['name']);
console.log(person.age);
console.log(person['age']);
console.log(person.isMarried);
console.log(person['isMarried']);
console.log(person.job);
console.log(person['job']);
console.log(person.car);
console.log(person['car']);
console.log(person.sym);
console.log(person['sym']);
console.log(person.bigInt);
console.log(person['bigInt']);
console.log(person.arr);
console.log(person['arr']);

//Arrays
var arr = [1,2,3,4,5];
console.log(arr[0]);
console.log(arr[1]);
console.log(arr[2]);
console.log(arr[3]);
console.log(arr[4]);
console.log(arr.length);   
console.log(arr[arr.length-1]);
console.log(arr.push(6));
console.log(arr.pop());
console.log(arr.shift());
console.log(arr.unshift(1));
console.log(arr.splice(2, 2, 9, 10));   
console.log(arr);
console.log(arr.slice(2, 4));
console.log(arr.concat([6, 7, 8]));
console.log(arr.reverse());
console.log(arr.sort());
console.log(arr.join());
console.log(arr.join(''));
console.log(arr.join('-'));
console.log(arr.toString());
console.log(arr.indexOf(3));
console.log(arr.includes(3));
console.log(arr.includes(11));
console.log(arr.every((x) => x>0));
console.log(arr.some((x) => x>5));

//Loops
var i = 0;
while(i<5) {
    console.log(i);
    i++;
}

for(var i=0; i<5; i++) {
    console.log(i);
}

var i = 0;
do {
    console.log(i);
    i++;
}
while(i<5);


//Break and Continue
for(var i=0; i<5; i++) {
    if(i==3) {
        break;
    }
    console.log(i);
}

for(var i=0; i<5; i++) {
    if(i==3) {
        continue;
    }
    console.log(i);
}

//Switch
var i = 2;
switch(i) {
    case 1:
        console.log("One");
        break;
    case 2:
        console.log("Two");
        break;
    case 3:
        console.log("Three");
        break;
    default:
        console.log("Default");
}

//Error Handling
try {
    console.log(10/0);
}
catch(err) {
    console.log(err.message);
}
finally {
    console.log("Finally");
}   

//Classes
class Person {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
    greet() {
        console.log("Hello, my name is " + this.name + " and I am " + this.age + " years old.");
    }
}

var person = new Person("John", 30);
person.greet();

//Inheritance
class Student extends Person {
    constructor(name, age, grade) {
        super(name, age);
        this.grade = grade;
    }
    greet() {
        console.log("Hello, my name is " + this.name + " and I am " + this.age + " years old and I am in grade " + this.grade);
    }
}

var student = new Student("John", 30, 5);
student.greet();


// Let vs Var vs Const

//Var 
var x = 5;
console.log(x); //5
{
    var x = 10;
    console.log(x); //10
}
console.log(x); //10

//Let
let y = 5;
console.log(y); //5
{
    let y = 10;
    console.log(y); //10
}
console.log(y); //5

//Const
const z = 5;
console.log(z); //5
{
    const z = 10;   
    console.log(z); //10
}
console.log(z); //5

console.log("hoisting");
//Hoisting

console.log(d);
var d = 5;
console.log(d);

// console.log(e);
// let e = 5;
// console.log(e);

// console.log(f);
// const f = 5;
// console.log(f);

//spread operator
console.log("spread operator");
let arr1 = [1, 2, 3];   
let arr2 = [...arr1, 4, 5, 6];
console.log(arr2);

let obj1 = {name: "John", age: 25};
let obj2 = {...obj , 'gender':male};
console.log(obj2);

//Rest operator
console.log("Rest operator");
function sum(...args){
    console.log(args); // add all the arguments
    let total = 0;
    for(let i of args){
        total += i;
    }
    console.log(total);
}

sum(1, 2, 3, 4, 5);

//Destructuring
console.log("Destructuring");
let arr = [1, 2, 3, 4, 5];
let [a, b, ...c] = arr;
console.log(a); //1
console.log(b); //2
console.log(c); //[3, 4, 5]




function add(...a){
    let sum = 0;
    for(let i of a){
        sum += i;
    }
    return sum;
}

//String operation

console.log("String operation");
let str = " Hello World How Are You? ";
console.log(str.startsWith("H")); //true
console.log(str.endsWith("o")); //true
console.log(str.includes("l")); //true
console.log(str.repeat(3)); //HelloHelloHello
log(str.padStart(10, "a")); //aaaaaHello
log(str.padEnd(10, "a")); //Helloaaaaa  
console.log(str.trim()); //Hello
console.log(str.trimStart()); //Hello
console.log(str.trimEnd()); //Hello
console.log(str.trimLeft()); //Hello
console.log(str.trimRight()); //Hello   
console.log(str.charAt(1)); //e
console.log(str.charCodeAt(1)); //101
console.log(str.concat("World")); //Hello World
console.log(str.indexOf("l")); //2
console.log(str.lastIndexOf("l")); //3
console.log(str.slice(1, 3)); //el
console.log(str.substring(1, 3)); //el
console.log(str.substr(1, 3)); //ell
console.log(str.split(" "));    //["Hello", "World", "How", "Are", "You?"]
console.log(str.toLowerCase()); //hello world how are you?
console.log(str.toUpperCase()); //HELLO WORLD HOW ARE YOU?
console.log(str.replace("Hello", "Hi")); //Hi World How Are You?
console.log(str.search("Hello")); //0   
console.log(str.match(/Hello/g)); //["Hello"]   
console.log(str.match(/Hello/gi)); //["Hello"]
console.log(str.match(/Hello/g).length); //1
console.log(str.match(/Hello/gi).length); //1
console.log(str.length); //24   
console.log(str.concat("World")); //Hello World

//Object operation
console.log("Object operation");
let obj = {name: "John", age: 25};
console.log(Object.keys(obj)); //["name", "age"]
console.log(Object.values(obj)); //["John", 25]
console.log(Object.entries(obj)); //[["name", "John"], ["age", 25]]
console.log (Object.assign ({}, obj)); //{name: "John", age: 25}  








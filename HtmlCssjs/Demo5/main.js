console.log("Hello World from main.js!");

// list and set in javascript
let list = [1, 2, 3, 4, 5];
let set = new Set(list);
console.log(set);

// map in javascript
let map = new Map();
map.set("name", "John");
map.set("age", 25);
console.log(map);

// WeakSet and WeakMap in javascript
let weakSet = new WeakSet();
let weakMap = new WeakMap();
let obj = { name: "John" };
weakSet.add(obj);
weakMap.set(obj, "John");
console.log(weakSet);

// list ,set and map functions
console.log(list.length);
console.log(set.size);
console.log(map.size); 
console.log(weakSet.size); // undefined
console.log(weakMap.size); // undefined

// for of loop
for (let val of list) {
  console.log(val);
}

for (let val of set) {
    console.log(val);
    }

for (let val of map) {
  console.log(val);
}

// for in loop
let obj1 = { name: "John", age: 25 };
for (let key in obj1) {
  console.log(key);
}   

// for each loop
list.forEach((val) => {
  console.log(val);
});

set.forEach((val) => {
  console.log(val);
}
);

map.forEach((val, key) => {
  console.log(key, val);
}   
);




